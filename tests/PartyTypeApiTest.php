<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PartyTypeApiTest extends TestCase
{
    use MakePartyTypeTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePartyType()
    {
        $partyType = $this->fakePartyTypeData();
        $this->json('POST', '/api/v1/partyTypes', $partyType);

        $this->assertApiResponse($partyType);
    }

    /**
     * @test
     */
    public function testReadPartyType()
    {
        $partyType = $this->makePartyType();
        $this->json('GET', '/api/v1/partyTypes/'.$partyType->id);

        $this->assertApiResponse($partyType->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePartyType()
    {
        $partyType = $this->makePartyType();
        $editedPartyType = $this->fakePartyTypeData();

        $this->json('PUT', '/api/v1/partyTypes/'.$partyType->id, $editedPartyType);

        $this->assertApiResponse($editedPartyType);
    }

    /**
     * @test
     */
    public function testDeletePartyType()
    {
        $partyType = $this->makePartyType();
        $this->json('DELETE', '/api/v1/partyTypes/'.$partyType->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/partyTypes/'.$partyType->id);

        $this->assertResponseStatus(404);
    }
}
