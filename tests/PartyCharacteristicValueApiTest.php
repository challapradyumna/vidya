<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PartyCharacteristicValueApiTest extends TestCase
{
    use MakePartyCharacteristicValueTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePartyCharacteristicValue()
    {
        $partyCharacteristicValue = $this->fakePartyCharacteristicValueData();
        $this->json('POST', '/api/v1/partyCharacteristicValues', $partyCharacteristicValue);

        $this->assertApiResponse($partyCharacteristicValue);
    }

    /**
     * @test
     */
    public function testReadPartyCharacteristicValue()
    {
        $partyCharacteristicValue = $this->makePartyCharacteristicValue();
        $this->json('GET', '/api/v1/partyCharacteristicValues/'.$partyCharacteristicValue->id);

        $this->assertApiResponse($partyCharacteristicValue->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePartyCharacteristicValue()
    {
        $partyCharacteristicValue = $this->makePartyCharacteristicValue();
        $editedPartyCharacteristicValue = $this->fakePartyCharacteristicValueData();

        $this->json('PUT', '/api/v1/partyCharacteristicValues/'.$partyCharacteristicValue->id, $editedPartyCharacteristicValue);

        $this->assertApiResponse($editedPartyCharacteristicValue);
    }

    /**
     * @test
     */
    public function testDeletePartyCharacteristicValue()
    {
        $partyCharacteristicValue = $this->makePartyCharacteristicValue();
        $this->json('DELETE', '/api/v1/partyCharacteristicValues/'.$partyCharacteristicValue->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/partyCharacteristicValues/'.$partyCharacteristicValue->id);

        $this->assertResponseStatus(404);
    }
}
