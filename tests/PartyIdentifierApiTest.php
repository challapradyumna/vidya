<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PartyIdentifierApiTest extends TestCase
{
    use MakePartyIdentifierTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePartyIdentifier()
    {
        $partyIdentifier = $this->fakePartyIdentifierData();
        $this->json('POST', '/api/v1/partyIdentifiers', $partyIdentifier);

        $this->assertApiResponse($partyIdentifier);
    }

    /**
     * @test
     */
    public function testReadPartyIdentifier()
    {
        $partyIdentifier = $this->makePartyIdentifier();
        $this->json('GET', '/api/v1/partyIdentifiers/'.$partyIdentifier->id);

        $this->assertApiResponse($partyIdentifier->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePartyIdentifier()
    {
        $partyIdentifier = $this->makePartyIdentifier();
        $editedPartyIdentifier = $this->fakePartyIdentifierData();

        $this->json('PUT', '/api/v1/partyIdentifiers/'.$partyIdentifier->id, $editedPartyIdentifier);

        $this->assertApiResponse($editedPartyIdentifier);
    }

    /**
     * @test
     */
    public function testDeletePartyIdentifier()
    {
        $partyIdentifier = $this->makePartyIdentifier();
        $this->json('DELETE', '/api/v1/partyIdentifiers/'.$partyIdentifier->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/partyIdentifiers/'.$partyIdentifier->id);

        $this->assertResponseStatus(404);
    }
}
