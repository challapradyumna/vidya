<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PartyIdentifierValueApiTest extends TestCase
{
    use MakePartyIdentifierValueTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePartyIdentifierValue()
    {
        $partyIdentifierValue = $this->fakePartyIdentifierValueData();
        $this->json('POST', '/api/v1/partyIdentifierValues', $partyIdentifierValue);

        $this->assertApiResponse($partyIdentifierValue);
    }

    /**
     * @test
     */
    public function testReadPartyIdentifierValue()
    {
        $partyIdentifierValue = $this->makePartyIdentifierValue();
        $this->json('GET', '/api/v1/partyIdentifierValues/'.$partyIdentifierValue->id);

        $this->assertApiResponse($partyIdentifierValue->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePartyIdentifierValue()
    {
        $partyIdentifierValue = $this->makePartyIdentifierValue();
        $editedPartyIdentifierValue = $this->fakePartyIdentifierValueData();

        $this->json('PUT', '/api/v1/partyIdentifierValues/'.$partyIdentifierValue->id, $editedPartyIdentifierValue);

        $this->assertApiResponse($editedPartyIdentifierValue);
    }

    /**
     * @test
     */
    public function testDeletePartyIdentifierValue()
    {
        $partyIdentifierValue = $this->makePartyIdentifierValue();
        $this->json('DELETE', '/api/v1/partyIdentifierValues/'.$partyIdentifierValue->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/partyIdentifierValues/'.$partyIdentifierValue->id);

        $this->assertResponseStatus(404);
    }
}
