<?php

use App\Models\PartyType;
use App\Repositories\PartyTypeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PartyTypeRepositoryTest extends TestCase
{
    use MakePartyTypeTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PartyTypeRepository
     */
    protected $partyTypeRepo;

    public function setUp()
    {
        parent::setUp();
        $this->partyTypeRepo = App::make(PartyTypeRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePartyType()
    {
        $partyType = $this->fakePartyTypeData();
        $createdPartyType = $this->partyTypeRepo->create($partyType);
        $createdPartyType = $createdPartyType->toArray();
        $this->assertArrayHasKey('id', $createdPartyType);
        $this->assertNotNull($createdPartyType['id'], 'Created PartyType must have id specified');
        $this->assertNotNull(PartyType::find($createdPartyType['id']), 'PartyType with given id must be in DB');
        $this->assertModelData($partyType, $createdPartyType);
    }

    /**
     * @test read
     */
    public function testReadPartyType()
    {
        $partyType = $this->makePartyType();
        $dbPartyType = $this->partyTypeRepo->find($partyType->id);
        $dbPartyType = $dbPartyType->toArray();
        $this->assertModelData($partyType->toArray(), $dbPartyType);
    }

    /**
     * @test update
     */
    public function testUpdatePartyType()
    {
        $partyType = $this->makePartyType();
        $fakePartyType = $this->fakePartyTypeData();
        $updatedPartyType = $this->partyTypeRepo->update($fakePartyType, $partyType->id);
        $this->assertModelData($fakePartyType, $updatedPartyType->toArray());
        $dbPartyType = $this->partyTypeRepo->find($partyType->id);
        $this->assertModelData($fakePartyType, $dbPartyType->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePartyType()
    {
        $partyType = $this->makePartyType();
        $resp = $this->partyTypeRepo->delete($partyType->id);
        $this->assertTrue($resp);
        $this->assertNull(PartyType::find($partyType->id), 'PartyType should not exist in DB');
    }
}
