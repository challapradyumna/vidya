<?php

use App\Models\PartyRelationshipType;
use App\Repositories\PartyRelationshipTypeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PartyRelationshipTypeRepositoryTest extends TestCase
{
    use MakePartyRelationshipTypeTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PartyRelationshipTypeRepository
     */
    protected $partyRelationshipTypeRepo;

    public function setUp()
    {
        parent::setUp();
        $this->partyRelationshipTypeRepo = App::make(PartyRelationshipTypeRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePartyRelationshipType()
    {
        $partyRelationshipType = $this->fakePartyRelationshipTypeData();
        $createdPartyRelationshipType = $this->partyRelationshipTypeRepo->create($partyRelationshipType);
        $createdPartyRelationshipType = $createdPartyRelationshipType->toArray();
        $this->assertArrayHasKey('id', $createdPartyRelationshipType);
        $this->assertNotNull($createdPartyRelationshipType['id'], 'Created PartyRelationshipType must have id specified');
        $this->assertNotNull(PartyRelationshipType::find($createdPartyRelationshipType['id']), 'PartyRelationshipType with given id must be in DB');
        $this->assertModelData($partyRelationshipType, $createdPartyRelationshipType);
    }

    /**
     * @test read
     */
    public function testReadPartyRelationshipType()
    {
        $partyRelationshipType = $this->makePartyRelationshipType();
        $dbPartyRelationshipType = $this->partyRelationshipTypeRepo->find($partyRelationshipType->id);
        $dbPartyRelationshipType = $dbPartyRelationshipType->toArray();
        $this->assertModelData($partyRelationshipType->toArray(), $dbPartyRelationshipType);
    }

    /**
     * @test update
     */
    public function testUpdatePartyRelationshipType()
    {
        $partyRelationshipType = $this->makePartyRelationshipType();
        $fakePartyRelationshipType = $this->fakePartyRelationshipTypeData();
        $updatedPartyRelationshipType = $this->partyRelationshipTypeRepo->update($fakePartyRelationshipType, $partyRelationshipType->id);
        $this->assertModelData($fakePartyRelationshipType, $updatedPartyRelationshipType->toArray());
        $dbPartyRelationshipType = $this->partyRelationshipTypeRepo->find($partyRelationshipType->id);
        $this->assertModelData($fakePartyRelationshipType, $dbPartyRelationshipType->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePartyRelationshipType()
    {
        $partyRelationshipType = $this->makePartyRelationshipType();
        $resp = $this->partyRelationshipTypeRepo->delete($partyRelationshipType->id);
        $this->assertTrue($resp);
        $this->assertNull(PartyRelationshipType::find($partyRelationshipType->id), 'PartyRelationshipType should not exist in DB');
    }
}
