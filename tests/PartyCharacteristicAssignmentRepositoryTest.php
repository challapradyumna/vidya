<?php

use App\Models\PartyCharacteristicAssignment;
use App\Repositories\PartyCharacteristicAssignmentRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PartyCharacteristicAssignmentRepositoryTest extends TestCase
{
    use MakePartyCharacteristicAssignmentTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PartyCharacteristicAssignmentRepository
     */
    protected $partyCharacteristicAssignmentRepo;

    public function setUp()
    {
        parent::setUp();
        $this->partyCharacteristicAssignmentRepo = App::make(PartyCharacteristicAssignmentRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePartyCharacteristicAssignment()
    {
        $partyCharacteristicAssignment = $this->fakePartyCharacteristicAssignmentData();
        $createdPartyCharacteristicAssignment = $this->partyCharacteristicAssignmentRepo->create($partyCharacteristicAssignment);
        $createdPartyCharacteristicAssignment = $createdPartyCharacteristicAssignment->toArray();
        $this->assertArrayHasKey('id', $createdPartyCharacteristicAssignment);
        $this->assertNotNull($createdPartyCharacteristicAssignment['id'], 'Created PartyCharacteristicAssignment must have id specified');
        $this->assertNotNull(PartyCharacteristicAssignment::find($createdPartyCharacteristicAssignment['id']), 'PartyCharacteristicAssignment with given id must be in DB');
        $this->assertModelData($partyCharacteristicAssignment, $createdPartyCharacteristicAssignment);
    }

    /**
     * @test read
     */
    public function testReadPartyCharacteristicAssignment()
    {
        $partyCharacteristicAssignment = $this->makePartyCharacteristicAssignment();
        $dbPartyCharacteristicAssignment = $this->partyCharacteristicAssignmentRepo->find($partyCharacteristicAssignment->id);
        $dbPartyCharacteristicAssignment = $dbPartyCharacteristicAssignment->toArray();
        $this->assertModelData($partyCharacteristicAssignment->toArray(), $dbPartyCharacteristicAssignment);
    }

    /**
     * @test update
     */
    public function testUpdatePartyCharacteristicAssignment()
    {
        $partyCharacteristicAssignment = $this->makePartyCharacteristicAssignment();
        $fakePartyCharacteristicAssignment = $this->fakePartyCharacteristicAssignmentData();
        $updatedPartyCharacteristicAssignment = $this->partyCharacteristicAssignmentRepo->update($fakePartyCharacteristicAssignment, $partyCharacteristicAssignment->id);
        $this->assertModelData($fakePartyCharacteristicAssignment, $updatedPartyCharacteristicAssignment->toArray());
        $dbPartyCharacteristicAssignment = $this->partyCharacteristicAssignmentRepo->find($partyCharacteristicAssignment->id);
        $this->assertModelData($fakePartyCharacteristicAssignment, $dbPartyCharacteristicAssignment->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePartyCharacteristicAssignment()
    {
        $partyCharacteristicAssignment = $this->makePartyCharacteristicAssignment();
        $resp = $this->partyCharacteristicAssignmentRepo->delete($partyCharacteristicAssignment->id);
        $this->assertTrue($resp);
        $this->assertNull(PartyCharacteristicAssignment::find($partyCharacteristicAssignment->id), 'PartyCharacteristicAssignment should not exist in DB');
    }
}
