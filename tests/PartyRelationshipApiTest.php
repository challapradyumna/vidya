<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PartyRelationshipApiTest extends TestCase
{
    use MakePartyRelationshipTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePartyRelationship()
    {
        $partyRelationship = $this->fakePartyRelationshipData();
        $this->json('POST', '/api/v1/partyRelationships', $partyRelationship);

        $this->assertApiResponse($partyRelationship);
    }

    /**
     * @test
     */
    public function testReadPartyRelationship()
    {
        $partyRelationship = $this->makePartyRelationship();
        $this->json('GET', '/api/v1/partyRelationships/'.$partyRelationship->id);

        $this->assertApiResponse($partyRelationship->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePartyRelationship()
    {
        $partyRelationship = $this->makePartyRelationship();
        $editedPartyRelationship = $this->fakePartyRelationshipData();

        $this->json('PUT', '/api/v1/partyRelationships/'.$partyRelationship->id, $editedPartyRelationship);

        $this->assertApiResponse($editedPartyRelationship);
    }

    /**
     * @test
     */
    public function testDeletePartyRelationship()
    {
        $partyRelationship = $this->makePartyRelationship();
        $this->json('DELETE', '/api/v1/partyRelationships/'.$partyRelationship->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/partyRelationships/'.$partyRelationship->id);

        $this->assertResponseStatus(404);
    }
}
