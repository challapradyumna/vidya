<?php

use App\Models\PartyRelationship;
use App\Repositories\PartyRelationshipRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PartyRelationshipRepositoryTest extends TestCase
{
    use MakePartyRelationshipTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PartyRelationshipRepository
     */
    protected $partyRelationshipRepo;

    public function setUp()
    {
        parent::setUp();
        $this->partyRelationshipRepo = App::make(PartyRelationshipRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePartyRelationship()
    {
        $partyRelationship = $this->fakePartyRelationshipData();
        $createdPartyRelationship = $this->partyRelationshipRepo->create($partyRelationship);
        $createdPartyRelationship = $createdPartyRelationship->toArray();
        $this->assertArrayHasKey('id', $createdPartyRelationship);
        $this->assertNotNull($createdPartyRelationship['id'], 'Created PartyRelationship must have id specified');
        $this->assertNotNull(PartyRelationship::find($createdPartyRelationship['id']), 'PartyRelationship with given id must be in DB');
        $this->assertModelData($partyRelationship, $createdPartyRelationship);
    }

    /**
     * @test read
     */
    public function testReadPartyRelationship()
    {
        $partyRelationship = $this->makePartyRelationship();
        $dbPartyRelationship = $this->partyRelationshipRepo->find($partyRelationship->id);
        $dbPartyRelationship = $dbPartyRelationship->toArray();
        $this->assertModelData($partyRelationship->toArray(), $dbPartyRelationship);
    }

    /**
     * @test update
     */
    public function testUpdatePartyRelationship()
    {
        $partyRelationship = $this->makePartyRelationship();
        $fakePartyRelationship = $this->fakePartyRelationshipData();
        $updatedPartyRelationship = $this->partyRelationshipRepo->update($fakePartyRelationship, $partyRelationship->id);
        $this->assertModelData($fakePartyRelationship, $updatedPartyRelationship->toArray());
        $dbPartyRelationship = $this->partyRelationshipRepo->find($partyRelationship->id);
        $this->assertModelData($fakePartyRelationship, $dbPartyRelationship->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePartyRelationship()
    {
        $partyRelationship = $this->makePartyRelationship();
        $resp = $this->partyRelationshipRepo->delete($partyRelationship->id);
        $this->assertTrue($resp);
        $this->assertNull(PartyRelationship::find($partyRelationship->id), 'PartyRelationship should not exist in DB');
    }
}
