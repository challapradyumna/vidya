<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PartyIdentifierAssignmentApiTest extends TestCase
{
    use MakePartyIdentifierAssignmentTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePartyIdentifierAssignment()
    {
        $partyIdentifierAssignment = $this->fakePartyIdentifierAssignmentData();
        $this->json('POST', '/api/v1/partyIdentifierAssignments', $partyIdentifierAssignment);

        $this->assertApiResponse($partyIdentifierAssignment);
    }

    /**
     * @test
     */
    public function testReadPartyIdentifierAssignment()
    {
        $partyIdentifierAssignment = $this->makePartyIdentifierAssignment();
        $this->json('GET', '/api/v1/partyIdentifierAssignments/'.$partyIdentifierAssignment->id);

        $this->assertApiResponse($partyIdentifierAssignment->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePartyIdentifierAssignment()
    {
        $partyIdentifierAssignment = $this->makePartyIdentifierAssignment();
        $editedPartyIdentifierAssignment = $this->fakePartyIdentifierAssignmentData();

        $this->json('PUT', '/api/v1/partyIdentifierAssignments/'.$partyIdentifierAssignment->id, $editedPartyIdentifierAssignment);

        $this->assertApiResponse($editedPartyIdentifierAssignment);
    }

    /**
     * @test
     */
    public function testDeletePartyIdentifierAssignment()
    {
        $partyIdentifierAssignment = $this->makePartyIdentifierAssignment();
        $this->json('DELETE', '/api/v1/partyIdentifierAssignments/'.$partyIdentifierAssignment->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/partyIdentifierAssignments/'.$partyIdentifierAssignment->id);

        $this->assertResponseStatus(404);
    }
}
