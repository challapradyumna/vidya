<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PartyCharacteristicApiTest extends TestCase
{
    use MakePartyCharacteristicTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePartyCharacteristic()
    {
        $partyCharacteristic = $this->fakePartyCharacteristicData();
        $this->json('POST', '/api/v1/partyCharacteristics', $partyCharacteristic);

        $this->assertApiResponse($partyCharacteristic);
    }

    /**
     * @test
     */
    public function testReadPartyCharacteristic()
    {
        $partyCharacteristic = $this->makePartyCharacteristic();
        $this->json('GET', '/api/v1/partyCharacteristics/'.$partyCharacteristic->id);

        $this->assertApiResponse($partyCharacteristic->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePartyCharacteristic()
    {
        $partyCharacteristic = $this->makePartyCharacteristic();
        $editedPartyCharacteristic = $this->fakePartyCharacteristicData();

        $this->json('PUT', '/api/v1/partyCharacteristics/'.$partyCharacteristic->id, $editedPartyCharacteristic);

        $this->assertApiResponse($editedPartyCharacteristic);
    }

    /**
     * @test
     */
    public function testDeletePartyCharacteristic()
    {
        $partyCharacteristic = $this->makePartyCharacteristic();
        $this->json('DELETE', '/api/v1/partyCharacteristics/'.$partyCharacteristic->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/partyCharacteristics/'.$partyCharacteristic->id);

        $this->assertResponseStatus(404);
    }
}
