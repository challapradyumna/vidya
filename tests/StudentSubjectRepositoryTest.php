<?php

use App\Models\StudentSubject;
use App\Repositories\StudentSubjectRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StudentSubjectRepositoryTest extends TestCase
{
    use MakeStudentSubjectTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var StudentSubjectRepository
     */
    protected $studentSubjectRepo;

    public function setUp()
    {
        parent::setUp();
        $this->studentSubjectRepo = App::make(StudentSubjectRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateStudentSubject()
    {
        $studentSubject = $this->fakeStudentSubjectData();
        $createdStudentSubject = $this->studentSubjectRepo->create($studentSubject);
        $createdStudentSubject = $createdStudentSubject->toArray();
        $this->assertArrayHasKey('id', $createdStudentSubject);
        $this->assertNotNull($createdStudentSubject['id'], 'Created StudentSubject must have id specified');
        $this->assertNotNull(StudentSubject::find($createdStudentSubject['id']), 'StudentSubject with given id must be in DB');
        $this->assertModelData($studentSubject, $createdStudentSubject);
    }

    /**
     * @test read
     */
    public function testReadStudentSubject()
    {
        $studentSubject = $this->makeStudentSubject();
        $dbStudentSubject = $this->studentSubjectRepo->find($studentSubject->id);
        $dbStudentSubject = $dbStudentSubject->toArray();
        $this->assertModelData($studentSubject->toArray(), $dbStudentSubject);
    }

    /**
     * @test update
     */
    public function testUpdateStudentSubject()
    {
        $studentSubject = $this->makeStudentSubject();
        $fakeStudentSubject = $this->fakeStudentSubjectData();
        $updatedStudentSubject = $this->studentSubjectRepo->update($fakeStudentSubject, $studentSubject->id);
        $this->assertModelData($fakeStudentSubject, $updatedStudentSubject->toArray());
        $dbStudentSubject = $this->studentSubjectRepo->find($studentSubject->id);
        $this->assertModelData($fakeStudentSubject, $dbStudentSubject->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteStudentSubject()
    {
        $studentSubject = $this->makeStudentSubject();
        $resp = $this->studentSubjectRepo->delete($studentSubject->id);
        $this->assertTrue($resp);
        $this->assertNull(StudentSubject::find($studentSubject->id), 'StudentSubject should not exist in DB');
    }
}
