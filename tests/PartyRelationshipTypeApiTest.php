<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PartyRelationshipTypeApiTest extends TestCase
{
    use MakePartyRelationshipTypeTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePartyRelationshipType()
    {
        $partyRelationshipType = $this->fakePartyRelationshipTypeData();
        $this->json('POST', '/api/v1/partyRelationshipTypes', $partyRelationshipType);

        $this->assertApiResponse($partyRelationshipType);
    }

    /**
     * @test
     */
    public function testReadPartyRelationshipType()
    {
        $partyRelationshipType = $this->makePartyRelationshipType();
        $this->json('GET', '/api/v1/partyRelationshipTypes/'.$partyRelationshipType->id);

        $this->assertApiResponse($partyRelationshipType->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePartyRelationshipType()
    {
        $partyRelationshipType = $this->makePartyRelationshipType();
        $editedPartyRelationshipType = $this->fakePartyRelationshipTypeData();

        $this->json('PUT', '/api/v1/partyRelationshipTypes/'.$partyRelationshipType->id, $editedPartyRelationshipType);

        $this->assertApiResponse($editedPartyRelationshipType);
    }

    /**
     * @test
     */
    public function testDeletePartyRelationshipType()
    {
        $partyRelationshipType = $this->makePartyRelationshipType();
        $this->json('DELETE', '/api/v1/partyRelationshipTypes/'.$partyRelationshipType->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/partyRelationshipTypes/'.$partyRelationshipType->id);

        $this->assertResponseStatus(404);
    }
}
