<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CourseStudentApiTest extends TestCase
{
    use MakeCourseStudentTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateCourseStudent()
    {
        $courseStudent = $this->fakeCourseStudentData();
        $this->json('POST', '/api/v1/courseStudents', $courseStudent);

        $this->assertApiResponse($courseStudent);
    }

    /**
     * @test
     */
    public function testReadCourseStudent()
    {
        $courseStudent = $this->makeCourseStudent();
        $this->json('GET', '/api/v1/courseStudents/'.$courseStudent->id);

        $this->assertApiResponse($courseStudent->toArray());
    }

    /**
     * @test
     */
    public function testUpdateCourseStudent()
    {
        $courseStudent = $this->makeCourseStudent();
        $editedCourseStudent = $this->fakeCourseStudentData();

        $this->json('PUT', '/api/v1/courseStudents/'.$courseStudent->id, $editedCourseStudent);

        $this->assertApiResponse($editedCourseStudent);
    }

    /**
     * @test
     */
    public function testDeleteCourseStudent()
    {
        $courseStudent = $this->makeCourseStudent();
        $this->json('DELETE', '/api/v1/courseStudents/'.$courseStudent->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/courseStudents/'.$courseStudent->id);

        $this->assertResponseStatus(404);
    }
}
