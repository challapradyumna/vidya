<?php

use App\Models\CourseStudent;
use App\Repositories\CourseStudentRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CourseStudentRepositoryTest extends TestCase
{
    use MakeCourseStudentTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var CourseStudentRepository
     */
    protected $courseStudentRepo;

    public function setUp()
    {
        parent::setUp();
        $this->courseStudentRepo = App::make(CourseStudentRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateCourseStudent()
    {
        $courseStudent = $this->fakeCourseStudentData();
        $createdCourseStudent = $this->courseStudentRepo->create($courseStudent);
        $createdCourseStudent = $createdCourseStudent->toArray();
        $this->assertArrayHasKey('id', $createdCourseStudent);
        $this->assertNotNull($createdCourseStudent['id'], 'Created CourseStudent must have id specified');
        $this->assertNotNull(CourseStudent::find($createdCourseStudent['id']), 'CourseStudent with given id must be in DB');
        $this->assertModelData($courseStudent, $createdCourseStudent);
    }

    /**
     * @test read
     */
    public function testReadCourseStudent()
    {
        $courseStudent = $this->makeCourseStudent();
        $dbCourseStudent = $this->courseStudentRepo->find($courseStudent->id);
        $dbCourseStudent = $dbCourseStudent->toArray();
        $this->assertModelData($courseStudent->toArray(), $dbCourseStudent);
    }

    /**
     * @test update
     */
    public function testUpdateCourseStudent()
    {
        $courseStudent = $this->makeCourseStudent();
        $fakeCourseStudent = $this->fakeCourseStudentData();
        $updatedCourseStudent = $this->courseStudentRepo->update($fakeCourseStudent, $courseStudent->id);
        $this->assertModelData($fakeCourseStudent, $updatedCourseStudent->toArray());
        $dbCourseStudent = $this->courseStudentRepo->find($courseStudent->id);
        $this->assertModelData($fakeCourseStudent, $dbCourseStudent->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteCourseStudent()
    {
        $courseStudent = $this->makeCourseStudent();
        $resp = $this->courseStudentRepo->delete($courseStudent->id);
        $this->assertTrue($resp);
        $this->assertNull(CourseStudent::find($courseStudent->id), 'CourseStudent should not exist in DB');
    }
}
