<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StudentSubjectApiTest extends TestCase
{
    use MakeStudentSubjectTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateStudentSubject()
    {
        $studentSubject = $this->fakeStudentSubjectData();
        $this->json('POST', '/api/v1/studentSubjects', $studentSubject);

        $this->assertApiResponse($studentSubject);
    }

    /**
     * @test
     */
    public function testReadStudentSubject()
    {
        $studentSubject = $this->makeStudentSubject();
        $this->json('GET', '/api/v1/studentSubjects/'.$studentSubject->id);

        $this->assertApiResponse($studentSubject->toArray());
    }

    /**
     * @test
     */
    public function testUpdateStudentSubject()
    {
        $studentSubject = $this->makeStudentSubject();
        $editedStudentSubject = $this->fakeStudentSubjectData();

        $this->json('PUT', '/api/v1/studentSubjects/'.$studentSubject->id, $editedStudentSubject);

        $this->assertApiResponse($editedStudentSubject);
    }

    /**
     * @test
     */
    public function testDeleteStudentSubject()
    {
        $studentSubject = $this->makeStudentSubject();
        $this->json('DELETE', '/api/v1/studentSubjects/'.$studentSubject->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/studentSubjects/'.$studentSubject->id);

        $this->assertResponseStatus(404);
    }
}
