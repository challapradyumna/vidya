<?php

use App\Models\PartyIdentifierValue;
use App\Repositories\PartyIdentifierValueRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PartyIdentifierValueRepositoryTest extends TestCase
{
    use MakePartyIdentifierValueTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PartyIdentifierValueRepository
     */
    protected $partyIdentifierValueRepo;

    public function setUp()
    {
        parent::setUp();
        $this->partyIdentifierValueRepo = App::make(PartyIdentifierValueRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePartyIdentifierValue()
    {
        $partyIdentifierValue = $this->fakePartyIdentifierValueData();
        $createdPartyIdentifierValue = $this->partyIdentifierValueRepo->create($partyIdentifierValue);
        $createdPartyIdentifierValue = $createdPartyIdentifierValue->toArray();
        $this->assertArrayHasKey('id', $createdPartyIdentifierValue);
        $this->assertNotNull($createdPartyIdentifierValue['id'], 'Created PartyIdentifierValue must have id specified');
        $this->assertNotNull(PartyIdentifierValue::find($createdPartyIdentifierValue['id']), 'PartyIdentifierValue with given id must be in DB');
        $this->assertModelData($partyIdentifierValue, $createdPartyIdentifierValue);
    }

    /**
     * @test read
     */
    public function testReadPartyIdentifierValue()
    {
        $partyIdentifierValue = $this->makePartyIdentifierValue();
        $dbPartyIdentifierValue = $this->partyIdentifierValueRepo->find($partyIdentifierValue->id);
        $dbPartyIdentifierValue = $dbPartyIdentifierValue->toArray();
        $this->assertModelData($partyIdentifierValue->toArray(), $dbPartyIdentifierValue);
    }

    /**
     * @test update
     */
    public function testUpdatePartyIdentifierValue()
    {
        $partyIdentifierValue = $this->makePartyIdentifierValue();
        $fakePartyIdentifierValue = $this->fakePartyIdentifierValueData();
        $updatedPartyIdentifierValue = $this->partyIdentifierValueRepo->update($fakePartyIdentifierValue, $partyIdentifierValue->id);
        $this->assertModelData($fakePartyIdentifierValue, $updatedPartyIdentifierValue->toArray());
        $dbPartyIdentifierValue = $this->partyIdentifierValueRepo->find($partyIdentifierValue->id);
        $this->assertModelData($fakePartyIdentifierValue, $dbPartyIdentifierValue->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePartyIdentifierValue()
    {
        $partyIdentifierValue = $this->makePartyIdentifierValue();
        $resp = $this->partyIdentifierValueRepo->delete($partyIdentifierValue->id);
        $this->assertTrue($resp);
        $this->assertNull(PartyIdentifierValue::find($partyIdentifierValue->id), 'PartyIdentifierValue should not exist in DB');
    }
}
