<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PartyCharacteristicAssignmentApiTest extends TestCase
{
    use MakePartyCharacteristicAssignmentTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePartyCharacteristicAssignment()
    {
        $partyCharacteristicAssignment = $this->fakePartyCharacteristicAssignmentData();
        $this->json('POST', '/api/v1/partyCharacteristicAssignments', $partyCharacteristicAssignment);

        $this->assertApiResponse($partyCharacteristicAssignment);
    }

    /**
     * @test
     */
    public function testReadPartyCharacteristicAssignment()
    {
        $partyCharacteristicAssignment = $this->makePartyCharacteristicAssignment();
        $this->json('GET', '/api/v1/partyCharacteristicAssignments/'.$partyCharacteristicAssignment->id);

        $this->assertApiResponse($partyCharacteristicAssignment->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePartyCharacteristicAssignment()
    {
        $partyCharacteristicAssignment = $this->makePartyCharacteristicAssignment();
        $editedPartyCharacteristicAssignment = $this->fakePartyCharacteristicAssignmentData();

        $this->json('PUT', '/api/v1/partyCharacteristicAssignments/'.$partyCharacteristicAssignment->id, $editedPartyCharacteristicAssignment);

        $this->assertApiResponse($editedPartyCharacteristicAssignment);
    }

    /**
     * @test
     */
    public function testDeletePartyCharacteristicAssignment()
    {
        $partyCharacteristicAssignment = $this->makePartyCharacteristicAssignment();
        $this->json('DELETE', '/api/v1/partyCharacteristicAssignments/'.$partyCharacteristicAssignment->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/partyCharacteristicAssignments/'.$partyCharacteristicAssignment->id);

        $this->assertResponseStatus(404);
    }
}
