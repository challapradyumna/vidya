<?php

use App\Models\PartyIdentifier;
use App\Repositories\PartyIdentifierRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PartyIdentifierRepositoryTest extends TestCase
{
    use MakePartyIdentifierTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PartyIdentifierRepository
     */
    protected $partyIdentifierRepo;

    public function setUp()
    {
        parent::setUp();
        $this->partyIdentifierRepo = App::make(PartyIdentifierRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePartyIdentifier()
    {
        $partyIdentifier = $this->fakePartyIdentifierData();
        $createdPartyIdentifier = $this->partyIdentifierRepo->create($partyIdentifier);
        $createdPartyIdentifier = $createdPartyIdentifier->toArray();
        $this->assertArrayHasKey('id', $createdPartyIdentifier);
        $this->assertNotNull($createdPartyIdentifier['id'], 'Created PartyIdentifier must have id specified');
        $this->assertNotNull(PartyIdentifier::find($createdPartyIdentifier['id']), 'PartyIdentifier with given id must be in DB');
        $this->assertModelData($partyIdentifier, $createdPartyIdentifier);
    }

    /**
     * @test read
     */
    public function testReadPartyIdentifier()
    {
        $partyIdentifier = $this->makePartyIdentifier();
        $dbPartyIdentifier = $this->partyIdentifierRepo->find($partyIdentifier->id);
        $dbPartyIdentifier = $dbPartyIdentifier->toArray();
        $this->assertModelData($partyIdentifier->toArray(), $dbPartyIdentifier);
    }

    /**
     * @test update
     */
    public function testUpdatePartyIdentifier()
    {
        $partyIdentifier = $this->makePartyIdentifier();
        $fakePartyIdentifier = $this->fakePartyIdentifierData();
        $updatedPartyIdentifier = $this->partyIdentifierRepo->update($fakePartyIdentifier, $partyIdentifier->id);
        $this->assertModelData($fakePartyIdentifier, $updatedPartyIdentifier->toArray());
        $dbPartyIdentifier = $this->partyIdentifierRepo->find($partyIdentifier->id);
        $this->assertModelData($fakePartyIdentifier, $dbPartyIdentifier->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePartyIdentifier()
    {
        $partyIdentifier = $this->makePartyIdentifier();
        $resp = $this->partyIdentifierRepo->delete($partyIdentifier->id);
        $this->assertTrue($resp);
        $this->assertNull(PartyIdentifier::find($partyIdentifier->id), 'PartyIdentifier should not exist in DB');
    }
}
