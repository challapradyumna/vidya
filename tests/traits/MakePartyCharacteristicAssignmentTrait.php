<?php

use Faker\Factory as Faker;
use App\Models\PartyCharacteristicAssignment;
use App\Repositories\PartyCharacteristicAssignmentRepository;

trait MakePartyCharacteristicAssignmentTrait
{
    /**
     * Create fake instance of PartyCharacteristicAssignment and save it in database
     *
     * @param array $partyCharacteristicAssignmentFields
     * @return PartyCharacteristicAssignment
     */
    public function makePartyCharacteristicAssignment($partyCharacteristicAssignmentFields = [])
    {
        /** @var PartyCharacteristicAssignmentRepository $partyCharacteristicAssignmentRepo */
        $partyCharacteristicAssignmentRepo = App::make(PartyCharacteristicAssignmentRepository::class);
        $theme = $this->fakePartyCharacteristicAssignmentData($partyCharacteristicAssignmentFields);
        return $partyCharacteristicAssignmentRepo->create($theme);
    }

    /**
     * Get fake instance of PartyCharacteristicAssignment
     *
     * @param array $partyCharacteristicAssignmentFields
     * @return PartyCharacteristicAssignment
     */
    public function fakePartyCharacteristicAssignment($partyCharacteristicAssignmentFields = [])
    {
        return new PartyCharacteristicAssignment($this->fakePartyCharacteristicAssignmentData($partyCharacteristicAssignmentFields));
    }

    /**
     * Get fake data of PartyCharacteristicAssignment
     *
     * @param array $postFields
     * @return array
     */
    public function fakePartyCharacteristicAssignmentData($partyCharacteristicAssignmentFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'party_type_id' => $fake->randomDigitNotNull,
            'party_characteristic_id' => $fake->randomDigitNotNull
        ], $partyCharacteristicAssignmentFields);
    }
}
