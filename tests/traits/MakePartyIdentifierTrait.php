<?php

use Faker\Factory as Faker;
use App\Models\PartyIdentifier;
use App\Repositories\PartyIdentifierRepository;

trait MakePartyIdentifierTrait
{
    /**
     * Create fake instance of PartyIdentifier and save it in database
     *
     * @param array $partyIdentifierFields
     * @return PartyIdentifier
     */
    public function makePartyIdentifier($partyIdentifierFields = [])
    {
        /** @var PartyIdentifierRepository $partyIdentifierRepo */
        $partyIdentifierRepo = App::make(PartyIdentifierRepository::class);
        $theme = $this->fakePartyIdentifierData($partyIdentifierFields);
        return $partyIdentifierRepo->create($theme);
    }

    /**
     * Get fake instance of PartyIdentifier
     *
     * @param array $partyIdentifierFields
     * @return PartyIdentifier
     */
    public function fakePartyIdentifier($partyIdentifierFields = [])
    {
        return new PartyIdentifier($this->fakePartyIdentifierData($partyIdentifierFields));
    }

    /**
     * Get fake data of PartyIdentifier
     *
     * @param array $postFields
     * @return array
     */
    public function fakePartyIdentifierData($partyIdentifierFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'description' => $fake->word,
            'issued_by' => $fake->randomDigitNotNull
        ], $partyIdentifierFields);
    }
}
