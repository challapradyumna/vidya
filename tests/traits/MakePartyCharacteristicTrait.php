<?php

use Faker\Factory as Faker;
use App\Models\PartyCharacteristic;
use App\Repositories\PartyCharacteristicRepository;

trait MakePartyCharacteristicTrait
{
    /**
     * Create fake instance of PartyCharacteristic and save it in database
     *
     * @param array $partyCharacteristicFields
     * @return PartyCharacteristic
     */
    public function makePartyCharacteristic($partyCharacteristicFields = [])
    {
        /** @var PartyCharacteristicRepository $partyCharacteristicRepo */
        $partyCharacteristicRepo = App::make(PartyCharacteristicRepository::class);
        $theme = $this->fakePartyCharacteristicData($partyCharacteristicFields);
        return $partyCharacteristicRepo->create($theme);
    }

    /**
     * Get fake instance of PartyCharacteristic
     *
     * @param array $partyCharacteristicFields
     * @return PartyCharacteristic
     */
    public function fakePartyCharacteristic($partyCharacteristicFields = [])
    {
        return new PartyCharacteristic($this->fakePartyCharacteristicData($partyCharacteristicFields));
    }

    /**
     * Get fake data of PartyCharacteristic
     *
     * @param array $postFields
     * @return array
     */
    public function fakePartyCharacteristicData($partyCharacteristicFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'description' => $fake->word,
            'default_value' => $fake->word,
            'defined_by' => $fake->randomDigitNotNull
        ], $partyCharacteristicFields);
    }
}
