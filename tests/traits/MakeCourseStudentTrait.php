<?php

use Faker\Factory as Faker;
use App\Models\CourseStudent;
use App\Repositories\CourseStudentRepository;

trait MakeCourseStudentTrait
{
    /**
     * Create fake instance of CourseStudent and save it in database
     *
     * @param array $courseStudentFields
     * @return CourseStudent
     */
    public function makeCourseStudent($courseStudentFields = [])
    {
        /** @var CourseStudentRepository $courseStudentRepo */
        $courseStudentRepo = App::make(CourseStudentRepository::class);
        $theme = $this->fakeCourseStudentData($courseStudentFields);
        return $courseStudentRepo->create($theme);
    }

    /**
     * Get fake instance of CourseStudent
     *
     * @param array $courseStudentFields
     * @return CourseStudent
     */
    public function fakeCourseStudent($courseStudentFields = [])
    {
        return new CourseStudent($this->fakeCourseStudentData($courseStudentFields));
    }

    /**
     * Get fake data of CourseStudent
     *
     * @param array $postFields
     * @return array
     */
    public function fakeCourseStudentData($courseStudentFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'course_id' => $fake->randomDigitNotNull,
            'student_id' => $fake->randomDigitNotNull
        ], $courseStudentFields);
    }
}
