<?php

use Faker\Factory as Faker;
use App\Models\PartyRelationship;
use App\Repositories\PartyRelationshipRepository;

trait MakePartyRelationshipTrait
{
    /**
     * Create fake instance of PartyRelationship and save it in database
     *
     * @param array $partyRelationshipFields
     * @return PartyRelationship
     */
    public function makePartyRelationship($partyRelationshipFields = [])
    {
        /** @var PartyRelationshipRepository $partyRelationshipRepo */
        $partyRelationshipRepo = App::make(PartyRelationshipRepository::class);
        $theme = $this->fakePartyRelationshipData($partyRelationshipFields);
        return $partyRelationshipRepo->create($theme);
    }

    /**
     * Get fake instance of PartyRelationship
     *
     * @param array $partyRelationshipFields
     * @return PartyRelationship
     */
    public function fakePartyRelationship($partyRelationshipFields = [])
    {
        return new PartyRelationship($this->fakePartyRelationshipData($partyRelationshipFields));
    }

    /**
     * Get fake data of PartyRelationship
     *
     * @param array $postFields
     * @return array
     */
    public function fakePartyRelationshipData($partyRelationshipFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'begin' => $fake->date('Y-m-d H:i:s'),
            'end' => $fake->date('Y-m-d H:i:s'),
            'from' => $fake->randomDigitNotNull,
            'to' => $fake->randomDigitNotNull,
            'party_relationship_type_id' => $fake->randomDigitNotNull
        ], $partyRelationshipFields);
    }
}
