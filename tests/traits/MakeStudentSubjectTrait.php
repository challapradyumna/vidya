<?php

use Faker\Factory as Faker;
use App\Models\StudentSubject;
use App\Repositories\StudentSubjectRepository;

trait MakeStudentSubjectTrait
{
    /**
     * Create fake instance of StudentSubject and save it in database
     *
     * @param array $studentSubjectFields
     * @return StudentSubject
     */
    public function makeStudentSubject($studentSubjectFields = [])
    {
        /** @var StudentSubjectRepository $studentSubjectRepo */
        $studentSubjectRepo = App::make(StudentSubjectRepository::class);
        $theme = $this->fakeStudentSubjectData($studentSubjectFields);
        return $studentSubjectRepo->create($theme);
    }

    /**
     * Get fake instance of StudentSubject
     *
     * @param array $studentSubjectFields
     * @return StudentSubject
     */
    public function fakeStudentSubject($studentSubjectFields = [])
    {
        return new StudentSubject($this->fakeStudentSubjectData($studentSubjectFields));
    }

    /**
     * Get fake data of StudentSubject
     *
     * @param array $postFields
     * @return array
     */
    public function fakeStudentSubjectData($studentSubjectFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'student_id' => $fake->randomDigitNotNull,
            'subject_id' => $fake->randomDigitNotNull
        ], $studentSubjectFields);
    }
}
