<?php

use Faker\Factory as Faker;
use App\Models\PartyIdentifierValue;
use App\Repositories\PartyIdentifierValueRepository;

trait MakePartyIdentifierValueTrait
{
    /**
     * Create fake instance of PartyIdentifierValue and save it in database
     *
     * @param array $partyIdentifierValueFields
     * @return PartyIdentifierValue
     */
    public function makePartyIdentifierValue($partyIdentifierValueFields = [])
    {
        /** @var PartyIdentifierValueRepository $partyIdentifierValueRepo */
        $partyIdentifierValueRepo = App::make(PartyIdentifierValueRepository::class);
        $theme = $this->fakePartyIdentifierValueData($partyIdentifierValueFields);
        return $partyIdentifierValueRepo->create($theme);
    }

    /**
     * Get fake instance of PartyIdentifierValue
     *
     * @param array $partyIdentifierValueFields
     * @return PartyIdentifierValue
     */
    public function fakePartyIdentifierValue($partyIdentifierValueFields = [])
    {
        return new PartyIdentifierValue($this->fakePartyIdentifierValueData($partyIdentifierValueFields));
    }

    /**
     * Get fake data of PartyIdentifierValue
     *
     * @param array $postFields
     * @return array
     */
    public function fakePartyIdentifierValueData($partyIdentifierValueFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'value' => $fake->word,
            'begin' => $fake->date('Y-m-d H:i:s'),
            'end' => $fake->date('Y-m-d H:i:s'),
            'comment' => $fake->word,
            'from' => $fake->randomDigitNotNull,
            'to' => $fake->randomDigitNotNull,
            'party_identifier_id' => $fake->randomDigitNotNull
        ], $partyIdentifierValueFields);
    }
}
