<?php

use Faker\Factory as Faker;
use App\Models\PartyCharacteristicValue;
use App\Repositories\PartyCharacteristicValueRepository;

trait MakePartyCharacteristicValueTrait
{
    /**
     * Create fake instance of PartyCharacteristicValue and save it in database
     *
     * @param array $partyCharacteristicValueFields
     * @return PartyCharacteristicValue
     */
    public function makePartyCharacteristicValue($partyCharacteristicValueFields = [])
    {
        /** @var PartyCharacteristicValueRepository $partyCharacteristicValueRepo */
        $partyCharacteristicValueRepo = App::make(PartyCharacteristicValueRepository::class);
        $theme = $this->fakePartyCharacteristicValueData($partyCharacteristicValueFields);
        return $partyCharacteristicValueRepo->create($theme);
    }

    /**
     * Get fake instance of PartyCharacteristicValue
     *
     * @param array $partyCharacteristicValueFields
     * @return PartyCharacteristicValue
     */
    public function fakePartyCharacteristicValue($partyCharacteristicValueFields = [])
    {
        return new PartyCharacteristicValue($this->fakePartyCharacteristicValueData($partyCharacteristicValueFields));
    }

    /**
     * Get fake data of PartyCharacteristicValue
     *
     * @param array $postFields
     * @return array
     */
    public function fakePartyCharacteristicValueData($partyCharacteristicValueFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'value' => $fake->word,
            'begin' => $fake->date('Y-m-d H:i:s'),
            'end' => $fake->date('Y-m-d H:i:s'),
            'by_party_id' => $fake->randomDigitNotNull,
            'characteristic_type_id' => $fake->randomDigitNotNull,
            'to_party_id' => $fake->randomDigitNotNull
        ], $partyCharacteristicValueFields);
    }
}
