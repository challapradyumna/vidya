<?php

use Faker\Factory as Faker;
use App\Models\PartyRelationshipType;
use App\Repositories\PartyRelationshipTypeRepository;

trait MakePartyRelationshipTypeTrait
{
    /**
     * Create fake instance of PartyRelationshipType and save it in database
     *
     * @param array $partyRelationshipTypeFields
     * @return PartyRelationshipType
     */
    public function makePartyRelationshipType($partyRelationshipTypeFields = [])
    {
        /** @var PartyRelationshipTypeRepository $partyRelationshipTypeRepo */
        $partyRelationshipTypeRepo = App::make(PartyRelationshipTypeRepository::class);
        $theme = $this->fakePartyRelationshipTypeData($partyRelationshipTypeFields);
        return $partyRelationshipTypeRepo->create($theme);
    }

    /**
     * Get fake instance of PartyRelationshipType
     *
     * @param array $partyRelationshipTypeFields
     * @return PartyRelationshipType
     */
    public function fakePartyRelationshipType($partyRelationshipTypeFields = [])
    {
        return new PartyRelationshipType($this->fakePartyRelationshipTypeData($partyRelationshipTypeFields));
    }

    /**
     * Get fake data of PartyRelationshipType
     *
     * @param array $postFields
     * @return array
     */
    public function fakePartyRelationshipTypeData($partyRelationshipTypeFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'description' => $fake->word
        ], $partyRelationshipTypeFields);
    }
}
