<?php

use Faker\Factory as Faker;
use App\Models\PartyIdentifierAssignment;
use App\Repositories\PartyIdentifierAssignmentRepository;

trait MakePartyIdentifierAssignmentTrait
{
    /**
     * Create fake instance of PartyIdentifierAssignment and save it in database
     *
     * @param array $partyIdentifierAssignmentFields
     * @return PartyIdentifierAssignment
     */
    public function makePartyIdentifierAssignment($partyIdentifierAssignmentFields = [])
    {
        /** @var PartyIdentifierAssignmentRepository $partyIdentifierAssignmentRepo */
        $partyIdentifierAssignmentRepo = App::make(PartyIdentifierAssignmentRepository::class);
        $theme = $this->fakePartyIdentifierAssignmentData($partyIdentifierAssignmentFields);
        return $partyIdentifierAssignmentRepo->create($theme);
    }

    /**
     * Get fake instance of PartyIdentifierAssignment
     *
     * @param array $partyIdentifierAssignmentFields
     * @return PartyIdentifierAssignment
     */
    public function fakePartyIdentifierAssignment($partyIdentifierAssignmentFields = [])
    {
        return new PartyIdentifierAssignment($this->fakePartyIdentifierAssignmentData($partyIdentifierAssignmentFields));
    }

    /**
     * Get fake data of PartyIdentifierAssignment
     *
     * @param array $postFields
     * @return array
     */
    public function fakePartyIdentifierAssignmentData($partyIdentifierAssignmentFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'party_type_id' => $fake->randomDigitNotNull,
            'party_identifier_id' => $fake->randomDigitNotNull
        ], $partyIdentifierAssignmentFields);
    }
}
