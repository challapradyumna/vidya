<?php

use App\Models\PartyIdentifierAssignment;
use App\Repositories\PartyIdentifierAssignmentRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PartyIdentifierAssignmentRepositoryTest extends TestCase
{
    use MakePartyIdentifierAssignmentTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PartyIdentifierAssignmentRepository
     */
    protected $partyIdentifierAssignmentRepo;

    public function setUp()
    {
        parent::setUp();
        $this->partyIdentifierAssignmentRepo = App::make(PartyIdentifierAssignmentRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePartyIdentifierAssignment()
    {
        $partyIdentifierAssignment = $this->fakePartyIdentifierAssignmentData();
        $createdPartyIdentifierAssignment = $this->partyIdentifierAssignmentRepo->create($partyIdentifierAssignment);
        $createdPartyIdentifierAssignment = $createdPartyIdentifierAssignment->toArray();
        $this->assertArrayHasKey('id', $createdPartyIdentifierAssignment);
        $this->assertNotNull($createdPartyIdentifierAssignment['id'], 'Created PartyIdentifierAssignment must have id specified');
        $this->assertNotNull(PartyIdentifierAssignment::find($createdPartyIdentifierAssignment['id']), 'PartyIdentifierAssignment with given id must be in DB');
        $this->assertModelData($partyIdentifierAssignment, $createdPartyIdentifierAssignment);
    }

    /**
     * @test read
     */
    public function testReadPartyIdentifierAssignment()
    {
        $partyIdentifierAssignment = $this->makePartyIdentifierAssignment();
        $dbPartyIdentifierAssignment = $this->partyIdentifierAssignmentRepo->find($partyIdentifierAssignment->id);
        $dbPartyIdentifierAssignment = $dbPartyIdentifierAssignment->toArray();
        $this->assertModelData($partyIdentifierAssignment->toArray(), $dbPartyIdentifierAssignment);
    }

    /**
     * @test update
     */
    public function testUpdatePartyIdentifierAssignment()
    {
        $partyIdentifierAssignment = $this->makePartyIdentifierAssignment();
        $fakePartyIdentifierAssignment = $this->fakePartyIdentifierAssignmentData();
        $updatedPartyIdentifierAssignment = $this->partyIdentifierAssignmentRepo->update($fakePartyIdentifierAssignment, $partyIdentifierAssignment->id);
        $this->assertModelData($fakePartyIdentifierAssignment, $updatedPartyIdentifierAssignment->toArray());
        $dbPartyIdentifierAssignment = $this->partyIdentifierAssignmentRepo->find($partyIdentifierAssignment->id);
        $this->assertModelData($fakePartyIdentifierAssignment, $dbPartyIdentifierAssignment->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePartyIdentifierAssignment()
    {
        $partyIdentifierAssignment = $this->makePartyIdentifierAssignment();
        $resp = $this->partyIdentifierAssignmentRepo->delete($partyIdentifierAssignment->id);
        $this->assertTrue($resp);
        $this->assertNull(PartyIdentifierAssignment::find($partyIdentifierAssignment->id), 'PartyIdentifierAssignment should not exist in DB');
    }
}
