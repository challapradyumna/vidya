<?php

use App\Models\PartyCharacteristic;
use App\Repositories\PartyCharacteristicRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PartyCharacteristicRepositoryTest extends TestCase
{
    use MakePartyCharacteristicTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PartyCharacteristicRepository
     */
    protected $partyCharacteristicRepo;

    public function setUp()
    {
        parent::setUp();
        $this->partyCharacteristicRepo = App::make(PartyCharacteristicRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePartyCharacteristic()
    {
        $partyCharacteristic = $this->fakePartyCharacteristicData();
        $createdPartyCharacteristic = $this->partyCharacteristicRepo->create($partyCharacteristic);
        $createdPartyCharacteristic = $createdPartyCharacteristic->toArray();
        $this->assertArrayHasKey('id', $createdPartyCharacteristic);
        $this->assertNotNull($createdPartyCharacteristic['id'], 'Created PartyCharacteristic must have id specified');
        $this->assertNotNull(PartyCharacteristic::find($createdPartyCharacteristic['id']), 'PartyCharacteristic with given id must be in DB');
        $this->assertModelData($partyCharacteristic, $createdPartyCharacteristic);
    }

    /**
     * @test read
     */
    public function testReadPartyCharacteristic()
    {
        $partyCharacteristic = $this->makePartyCharacteristic();
        $dbPartyCharacteristic = $this->partyCharacteristicRepo->find($partyCharacteristic->id);
        $dbPartyCharacteristic = $dbPartyCharacteristic->toArray();
        $this->assertModelData($partyCharacteristic->toArray(), $dbPartyCharacteristic);
    }

    /**
     * @test update
     */
    public function testUpdatePartyCharacteristic()
    {
        $partyCharacteristic = $this->makePartyCharacteristic();
        $fakePartyCharacteristic = $this->fakePartyCharacteristicData();
        $updatedPartyCharacteristic = $this->partyCharacteristicRepo->update($fakePartyCharacteristic, $partyCharacteristic->id);
        $this->assertModelData($fakePartyCharacteristic, $updatedPartyCharacteristic->toArray());
        $dbPartyCharacteristic = $this->partyCharacteristicRepo->find($partyCharacteristic->id);
        $this->assertModelData($fakePartyCharacteristic, $dbPartyCharacteristic->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePartyCharacteristic()
    {
        $partyCharacteristic = $this->makePartyCharacteristic();
        $resp = $this->partyCharacteristicRepo->delete($partyCharacteristic->id);
        $this->assertTrue($resp);
        $this->assertNull(PartyCharacteristic::find($partyCharacteristic->id), 'PartyCharacteristic should not exist in DB');
    }
}
