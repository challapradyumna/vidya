<?php

use App\Models\PartyCharacteristicValue;
use App\Repositories\PartyCharacteristicValueRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PartyCharacteristicValueRepositoryTest extends TestCase
{
    use MakePartyCharacteristicValueTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PartyCharacteristicValueRepository
     */
    protected $partyCharacteristicValueRepo;

    public function setUp()
    {
        parent::setUp();
        $this->partyCharacteristicValueRepo = App::make(PartyCharacteristicValueRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePartyCharacteristicValue()
    {
        $partyCharacteristicValue = $this->fakePartyCharacteristicValueData();
        $createdPartyCharacteristicValue = $this->partyCharacteristicValueRepo->create($partyCharacteristicValue);
        $createdPartyCharacteristicValue = $createdPartyCharacteristicValue->toArray();
        $this->assertArrayHasKey('id', $createdPartyCharacteristicValue);
        $this->assertNotNull($createdPartyCharacteristicValue['id'], 'Created PartyCharacteristicValue must have id specified');
        $this->assertNotNull(PartyCharacteristicValue::find($createdPartyCharacteristicValue['id']), 'PartyCharacteristicValue with given id must be in DB');
        $this->assertModelData($partyCharacteristicValue, $createdPartyCharacteristicValue);
    }

    /**
     * @test read
     */
    public function testReadPartyCharacteristicValue()
    {
        $partyCharacteristicValue = $this->makePartyCharacteristicValue();
        $dbPartyCharacteristicValue = $this->partyCharacteristicValueRepo->find($partyCharacteristicValue->id);
        $dbPartyCharacteristicValue = $dbPartyCharacteristicValue->toArray();
        $this->assertModelData($partyCharacteristicValue->toArray(), $dbPartyCharacteristicValue);
    }

    /**
     * @test update
     */
    public function testUpdatePartyCharacteristicValue()
    {
        $partyCharacteristicValue = $this->makePartyCharacteristicValue();
        $fakePartyCharacteristicValue = $this->fakePartyCharacteristicValueData();
        $updatedPartyCharacteristicValue = $this->partyCharacteristicValueRepo->update($fakePartyCharacteristicValue, $partyCharacteristicValue->id);
        $this->assertModelData($fakePartyCharacteristicValue, $updatedPartyCharacteristicValue->toArray());
        $dbPartyCharacteristicValue = $this->partyCharacteristicValueRepo->find($partyCharacteristicValue->id);
        $this->assertModelData($fakePartyCharacteristicValue, $dbPartyCharacteristicValue->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePartyCharacteristicValue()
    {
        $partyCharacteristicValue = $this->makePartyCharacteristicValue();
        $resp = $this->partyCharacteristicValueRepo->delete($partyCharacteristicValue->id);
        $this->assertTrue($resp);
        $this->assertNull(PartyCharacteristicValue::find($partyCharacteristicValue->id), 'PartyCharacteristicValue should not exist in DB');
    }
}
