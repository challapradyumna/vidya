<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateStudentSubjectAPIRequest;
use App\Http\Requests\API\UpdateStudentSubjectAPIRequest;
use App\Models\StudentSubject;
use App\Repositories\StudentSubjectRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class StudentSubjectController
 * @package App\Http\Controllers\API
 */

class StudentSubjectAPIController extends AppBaseController
{
    /** @var  StudentSubjectRepository */
    private $studentSubjectRepository;

    public function __construct(StudentSubjectRepository $studentSubjectRepo)
    {
        $this->studentSubjectRepository = $studentSubjectRepo;
    }

    /**
     * Display a listing of the StudentSubject.
     * GET|HEAD /studentSubjects
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->studentSubjectRepository->pushCriteria(new RequestCriteria($request));
        $this->studentSubjectRepository->pushCriteria(new LimitOffsetCriteria($request));
        $studentSubjects = $this->studentSubjectRepository->all();

        return $this->sendResponse($studentSubjects->toArray(), 'Student Subjects retrieved successfully');
    }

    /**
     * Store a newly created StudentSubject in storage.
     * POST /studentSubjects
     *
     * @param CreateStudentSubjectAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateStudentSubjectAPIRequest $request)
    {
        $input = $request->all();

        $studentSubjects = $this->studentSubjectRepository->create($input);

        return $this->sendResponse($studentSubjects->toArray(), 'Student Subject saved successfully');
    }

    /**
     * Display the specified StudentSubject.
     * GET|HEAD /studentSubjects/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var StudentSubject $studentSubject */
        $studentSubject = $this->studentSubjectRepository->findWithoutFail($id);

        if (empty($studentSubject)) {
            return $this->sendError('Student Subject not found');
        }

        return $this->sendResponse($studentSubject->toArray(), 'Student Subject retrieved successfully');
    }

    /**
     * Update the specified StudentSubject in storage.
     * PUT/PATCH /studentSubjects/{id}
     *
     * @param  int $id
     * @param UpdateStudentSubjectAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStudentSubjectAPIRequest $request)
    {
        $input = $request->all();

        /** @var StudentSubject $studentSubject */
        $studentSubject = $this->studentSubjectRepository->findWithoutFail($id);

        if (empty($studentSubject)) {
            return $this->sendError('Student Subject not found');
        }

        $studentSubject = $this->studentSubjectRepository->update($input, $id);

        return $this->sendResponse($studentSubject->toArray(), 'StudentSubject updated successfully');
    }

    /**
     * Remove the specified StudentSubject from storage.
     * DELETE /studentSubjects/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var StudentSubject $studentSubject */
        $studentSubject = $this->studentSubjectRepository->findWithoutFail($id);

        if (empty($studentSubject)) {
            return $this->sendError('Student Subject not found');
        }

        $studentSubject->delete();

        return $this->sendResponse($id, 'Student Subject deleted successfully');
    }
}
