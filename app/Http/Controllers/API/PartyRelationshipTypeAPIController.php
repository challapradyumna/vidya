<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePartyRelationshipTypeAPIRequest;
use App\Http\Requests\API\UpdatePartyRelationshipTypeAPIRequest;
use App\Models\PartyRelationshipType;
use App\Repositories\PartyRelationshipTypeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PartyRelationshipTypeController
 * @package App\Http\Controllers\API
 */

class PartyRelationshipTypeAPIController extends AppBaseController
{
    /** @var  PartyRelationshipTypeRepository */
    private $partyRelationshipTypeRepository;

    public function __construct(PartyRelationshipTypeRepository $partyRelationshipTypeRepo)
    {
        $this->partyRelationshipTypeRepository = $partyRelationshipTypeRepo;
    }

    /**
     * Display a listing of the PartyRelationshipType.
     * GET|HEAD /partyRelationshipTypes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->partyRelationshipTypeRepository->pushCriteria(new RequestCriteria($request));
        $this->partyRelationshipTypeRepository->pushCriteria(new LimitOffsetCriteria($request));
        $partyRelationshipTypes = $this->partyRelationshipTypeRepository->all();

        return $this->sendResponse($partyRelationshipTypes->toArray(), 'Party Relationship Types retrieved successfully');
    }

    /**
     * Store a newly created PartyRelationshipType in storage.
     * POST /partyRelationshipTypes
     *
     * @param CreatePartyRelationshipTypeAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePartyRelationshipTypeAPIRequest $request)
    {
        $input = $request->all();

        $partyRelationshipTypes = $this->partyRelationshipTypeRepository->create($input);

        return $this->sendResponse($partyRelationshipTypes->toArray(), 'Party Relationship Type saved successfully');
    }

    /**
     * Display the specified PartyRelationshipType.
     * GET|HEAD /partyRelationshipTypes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var PartyRelationshipType $partyRelationshipType */
        $partyRelationshipType = $this->partyRelationshipTypeRepository->findWithoutFail($id);

        if (empty($partyRelationshipType)) {
            return $this->sendError('Party Relationship Type not found');
        }

        return $this->sendResponse($partyRelationshipType->toArray(), 'Party Relationship Type retrieved successfully');
    }

    /**
     * Update the specified PartyRelationshipType in storage.
     * PUT/PATCH /partyRelationshipTypes/{id}
     *
     * @param  int $id
     * @param UpdatePartyRelationshipTypeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePartyRelationshipTypeAPIRequest $request)
    {
        $input = $request->all();

        /** @var PartyRelationshipType $partyRelationshipType */
        $partyRelationshipType = $this->partyRelationshipTypeRepository->findWithoutFail($id);

        if (empty($partyRelationshipType)) {
            return $this->sendError('Party Relationship Type not found');
        }

        $partyRelationshipType = $this->partyRelationshipTypeRepository->update($input, $id);

        return $this->sendResponse($partyRelationshipType->toArray(), 'PartyRelationshipType updated successfully');
    }

    /**
     * Remove the specified PartyRelationshipType from storage.
     * DELETE /partyRelationshipTypes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var PartyRelationshipType $partyRelationshipType */
        $partyRelationshipType = $this->partyRelationshipTypeRepository->findWithoutFail($id);

        if (empty($partyRelationshipType)) {
            return $this->sendError('Party Relationship Type not found');
        }

        $partyRelationshipType->delete();

        return $this->sendResponse($id, 'Party Relationship Type deleted successfully');
    }
}
