<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePartyIdentifierValueAPIRequest;
use App\Http\Requests\API\UpdatePartyIdentifierValueAPIRequest;
use App\Models\PartyIdentifierValue;
use App\Repositories\PartyIdentifierValueRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PartyIdentifierValueController
 * @package App\Http\Controllers\API
 */

class PartyIdentifierValueAPIController extends AppBaseController
{
    /** @var  PartyIdentifierValueRepository */
    private $partyIdentifierValueRepository;

    public function __construct(PartyIdentifierValueRepository $partyIdentifierValueRepo)
    {
        $this->partyIdentifierValueRepository = $partyIdentifierValueRepo;
    }

    /**
     * Display a listing of the PartyIdentifierValue.
     * GET|HEAD /partyIdentifierValues
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->partyIdentifierValueRepository->pushCriteria(new RequestCriteria($request));
        $this->partyIdentifierValueRepository->pushCriteria(new LimitOffsetCriteria($request));
        $partyIdentifierValues = $this->partyIdentifierValueRepository->paginate(10);

        return $this->sendResponse($partyIdentifierValues->toArray(), 'Party Identifier Values retrieved successfully');
    }

    /**
     * Store a newly created PartyIdentifierValue in storage.
     * POST /partyIdentifierValues
     *
     * @param CreatePartyIdentifierValueAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePartyIdentifierValueAPIRequest $request)
    {
        $input = $request->all();

        $partyIdentifierValues = $this->partyIdentifierValueRepository->create($input);

        return $this->sendResponse($partyIdentifierValues->toArray(), 'Party Identifier Value saved successfully');
    }

    /**
     * Display the specified PartyIdentifierValue.
     * GET|HEAD /partyIdentifierValues/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var PartyIdentifierValue $partyIdentifierValue */
        $partyIdentifierValue = $this->partyIdentifierValueRepository->findWithoutFail($id);

        if (empty($partyIdentifierValue)) {
            return $this->sendError('Party Identifier Value not found');
        }

        return $this->sendResponse($partyIdentifierValue->toArray(), 'Party Identifier Value retrieved successfully');
    }

    /**
     * Update the specified PartyIdentifierValue in storage.
     * PUT/PATCH /partyIdentifierValues/{id}
     *
     * @param  int $id
     * @param UpdatePartyIdentifierValueAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePartyIdentifierValueAPIRequest $request)
    {
        $input = $request->all();

        /** @var PartyIdentifierValue $partyIdentifierValue */
        $partyIdentifierValue = $this->partyIdentifierValueRepository->findWithoutFail($id);

        if (empty($partyIdentifierValue)) {
            return $this->sendError('Party Identifier Value not found');
        }

        $partyIdentifierValue = $this->partyIdentifierValueRepository->update($input, $id);

        return $this->sendResponse($partyIdentifierValue->toArray(), 'PartyIdentifierValue updated successfully');
    }

    /**
     * Remove the specified PartyIdentifierValue from storage.
     * DELETE /partyIdentifierValues/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var PartyIdentifierValue $partyIdentifierValue */
        $partyIdentifierValue = $this->partyIdentifierValueRepository->findWithoutFail($id);

        if (empty($partyIdentifierValue)) {
            return $this->sendError('Party Identifier Value not found');
        }

        $partyIdentifierValue->delete();

        return $this->sendResponse($id, 'Party Identifier Value deleted successfully');
    }
}
