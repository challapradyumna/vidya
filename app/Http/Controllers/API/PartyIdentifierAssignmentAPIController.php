<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePartyIdentifierAssignmentAPIRequest;
use App\Http\Requests\API\UpdatePartyIdentifierAssignmentAPIRequest;
use App\Models\PartyIdentifierAssignment;
use App\Repositories\PartyIdentifierAssignmentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PartyIdentifierAssignmentController
 * @package App\Http\Controllers\API
 */

class PartyIdentifierAssignmentAPIController extends AppBaseController
{
    /** @var  PartyIdentifierAssignmentRepository */
    private $partyIdentifierAssignmentRepository;

    public function __construct(PartyIdentifierAssignmentRepository $partyIdentifierAssignmentRepo)
    {
        $this->partyIdentifierAssignmentRepository = $partyIdentifierAssignmentRepo;
    }

    /**
     * Display a listing of the PartyIdentifierAssignment.
     * GET|HEAD /partyIdentifierAssignments
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->partyIdentifierAssignmentRepository->pushCriteria(new RequestCriteria($request));
        $this->partyIdentifierAssignmentRepository->pushCriteria(new LimitOffsetCriteria($request));
        $partyIdentifierAssignments = $this->partyIdentifierAssignmentRepository->all();

        return $this->sendResponse($partyIdentifierAssignments->toArray(), 'Party Identifier Assignments retrieved successfully');
    }

    /**
     * Store a newly created PartyIdentifierAssignment in storage.
     * POST /partyIdentifierAssignments
     *
     * @param CreatePartyIdentifierAssignmentAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePartyIdentifierAssignmentAPIRequest $request)
    {
        $input = $request->all();

        $partyIdentifierAssignments = $this->partyIdentifierAssignmentRepository->create($input);

        return $this->sendResponse($partyIdentifierAssignments->toArray(), 'Party Identifier Assignment saved successfully');
    }

    /**
     * Display the specified PartyIdentifierAssignment.
     * GET|HEAD /partyIdentifierAssignments/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var PartyIdentifierAssignment $partyIdentifierAssignment */
        $partyIdentifierAssignment = $this->partyIdentifierAssignmentRepository->findWithoutFail($id);

        if (empty($partyIdentifierAssignment)) {
            return $this->sendError('Party Identifier Assignment not found');
        }

        return $this->sendResponse($partyIdentifierAssignment->toArray(), 'Party Identifier Assignment retrieved successfully');
    }

    /**
     * Update the specified PartyIdentifierAssignment in storage.
     * PUT/PATCH /partyIdentifierAssignments/{id}
     *
     * @param  int $id
     * @param UpdatePartyIdentifierAssignmentAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePartyIdentifierAssignmentAPIRequest $request)
    {
        $input = $request->all();

        /** @var PartyIdentifierAssignment $partyIdentifierAssignment */
        $partyIdentifierAssignment = $this->partyIdentifierAssignmentRepository->findWithoutFail($id);

        if (empty($partyIdentifierAssignment)) {
            return $this->sendError('Party Identifier Assignment not found');
        }

        $partyIdentifierAssignment = $this->partyIdentifierAssignmentRepository->update($input, $id);

        return $this->sendResponse($partyIdentifierAssignment->toArray(), 'PartyIdentifierAssignment updated successfully');
    }

    /**
     * Remove the specified PartyIdentifierAssignment from storage.
     * DELETE /partyIdentifierAssignments/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var PartyIdentifierAssignment $partyIdentifierAssignment */
        $partyIdentifierAssignment = $this->partyIdentifierAssignmentRepository->findWithoutFail($id);

        if (empty($partyIdentifierAssignment)) {
            return $this->sendError('Party Identifier Assignment not found');
        }

        $partyIdentifierAssignment->delete();

        return $this->sendResponse($id, 'Party Identifier Assignment deleted successfully');
    }
}
