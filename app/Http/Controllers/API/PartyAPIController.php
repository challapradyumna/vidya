<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePartyAPIRequest;
use App\Http\Requests\API\UpdatePartyAPIRequest;
use App\Models\Party;
use App\Repositories\PartyRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as Controller;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PartyController
 * @package App\Http\Controllers\API
 */

class PartyAPIController extends Controller
{
    /** @var  PartyRepository */
    private $partyRepository;

    public function __construct(PartyRepository $partyRepo)
    {
        $this->partyRepository = $partyRepo;
    }

    /**
     * Display a listing of the Party.
     * GET|HEAD /parties
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->partyRepository->pushCriteria(new RequestCriteria($request));
        $this->partyRepository->pushCriteria(new LimitOffsetCriteria($request));
        $parties = $this->partyRepository->paginate(10);
        return $this->sendResponse($parties->toArray(), 'Parties retrieved successfully');
    }

    /**
     * Store a newly created Party in storage.
     * POST /parties
     *
     * @param CreatePartyAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePartyAPIRequest $request)
    {
        $input = $request->all();

        $parties = $this->partyRepository->create($input);

        return $this->sendResponse($parties->toArray(), 'Party saved successfully');
    }

    /**
     * Display the specified Party.
     * GET|HEAD /parties/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Party $party */
        $party = $this->partyRepository->findWithoutFail($id);

        if (empty($party)) {
            return $this->sendError('Party not found');
        }
        return $this->sendResponse($party->toArray(), 'Party retrieved successfully');
    }

    /**
     * Update the specified Party in storage.
     * PUT/PATCH /parties/{id}
     *
     * @param  int $id
     * @param UpdatePartyAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePartyAPIRequest $request)
    {
        $input = $request->all();

        /** @var Party $party */
        $party = $this->partyRepository->findWithoutFail($id);

        if (empty($party)) {
            return $this->sendError('Party not found');
        }

        $party = $this->partyRepository->update($input, $id);

        return $this->sendResponse($party->toArray(), 'Party updated successfully');
    }

    /**
     * Remove the specified Party from storage.
     * DELETE /parties/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Party $party */
        $party = $this->partyRepository->findWithoutFail($id);

        if (empty($party)) {
            return $this->sendError('Party not found');
        }

        $party->delete();

        return $this->sendResponse($id, 'Party deleted successfully');
    }
}
