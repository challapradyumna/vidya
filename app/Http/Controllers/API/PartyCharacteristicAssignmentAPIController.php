<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePartyCharacteristicAssignmentAPIRequest;
use App\Http\Requests\API\UpdatePartyCharacteristicAssignmentAPIRequest;
use App\Models\PartyCharacteristicAssignment;
use App\Repositories\PartyCharacteristicAssignmentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PartyCharacteristicAssignmentController
 * @package App\Http\Controllers\API
 */

class PartyCharacteristicAssignmentAPIController extends AppBaseController
{
    /** @var  PartyCharacteristicAssignmentRepository */
    private $partyCharacteristicAssignmentRepository;

    public function __construct(PartyCharacteristicAssignmentRepository $partyCharacteristicAssignmentRepo)
    {
        $this->partyCharacteristicAssignmentRepository = $partyCharacteristicAssignmentRepo;
    }

    /**
     * Display a listing of the PartyCharacteristicAssignment.
     * GET|HEAD /partyCharacteristicAssignments
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->partyCharacteristicAssignmentRepository->pushCriteria(new RequestCriteria($request));
        $this->partyCharacteristicAssignmentRepository->pushCriteria(new LimitOffsetCriteria($request));
        $partyCharacteristicAssignments = $this->partyCharacteristicAssignmentRepository->all();

        return $this->sendResponse($partyCharacteristicAssignments->toArray(), 'Party Characteristic Assignments retrieved successfully');
    }

    /**
     * Store a newly created PartyCharacteristicAssignment in storage.
     * POST /partyCharacteristicAssignments
     *
     * @param CreatePartyCharacteristicAssignmentAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePartyCharacteristicAssignmentAPIRequest $request)
    {
        $input = $request->all();

        $partyCharacteristicAssignments = $this->partyCharacteristicAssignmentRepository->create($input);

        return $this->sendResponse($partyCharacteristicAssignments->toArray(), 'Party Characteristic Assignment saved successfully');
    }

    /**
     * Display the specified PartyCharacteristicAssignment.
     * GET|HEAD /partyCharacteristicAssignments/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var PartyCharacteristicAssignment $partyCharacteristicAssignment */
        $partyCharacteristicAssignment = $this->partyCharacteristicAssignmentRepository->findWithoutFail($id);

        if (empty($partyCharacteristicAssignment)) {
            return $this->sendError('Party Characteristic Assignment not found');
        }

        return $this->sendResponse($partyCharacteristicAssignment->toArray(), 'Party Characteristic Assignment retrieved successfully');
    }

    /**
     * Update the specified PartyCharacteristicAssignment in storage.
     * PUT/PATCH /partyCharacteristicAssignments/{id}
     *
     * @param  int $id
     * @param UpdatePartyCharacteristicAssignmentAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePartyCharacteristicAssignmentAPIRequest $request)
    {
        $input = $request->all();

        /** @var PartyCharacteristicAssignment $partyCharacteristicAssignment */
        $partyCharacteristicAssignment = $this->partyCharacteristicAssignmentRepository->findWithoutFail($id);

        if (empty($partyCharacteristicAssignment)) {
            return $this->sendError('Party Characteristic Assignment not found');
        }

        $partyCharacteristicAssignment = $this->partyCharacteristicAssignmentRepository->update($input, $id);

        return $this->sendResponse($partyCharacteristicAssignment->toArray(), 'PartyCharacteristicAssignment updated successfully');
    }

    /**
     * Remove the specified PartyCharacteristicAssignment from storage.
     * DELETE /partyCharacteristicAssignments/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var PartyCharacteristicAssignment $partyCharacteristicAssignment */
        $partyCharacteristicAssignment = $this->partyCharacteristicAssignmentRepository->findWithoutFail($id);

        if (empty($partyCharacteristicAssignment)) {
            return $this->sendError('Party Characteristic Assignment not found');
        }

        $partyCharacteristicAssignment->delete();

        return $this->sendResponse($id, 'Party Characteristic Assignment deleted successfully');
    }
}
