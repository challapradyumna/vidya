<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCourseStudentAPIRequest;
use App\Http\Requests\API\UpdateCourseStudentAPIRequest;
use App\Models\CourseStudent;
use App\Repositories\CourseStudentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class CourseStudentController
 * @package App\Http\Controllers\API
 */

class CourseStudentAPIController extends AppBaseController
{
    /** @var  CourseStudentRepository */
    private $courseStudentRepository;

    public function __construct(CourseStudentRepository $courseStudentRepo)
    {
        $this->courseStudentRepository = $courseStudentRepo;
    }

    /**
     * Display a listing of the CourseStudent.
     * GET|HEAD /courseStudents
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->courseStudentRepository->pushCriteria(new RequestCriteria($request));
        $this->courseStudentRepository->pushCriteria(new LimitOffsetCriteria($request));
        $courseStudents = $this->courseStudentRepository->all();

        return $this->sendResponse($courseStudents->toArray(), 'Course Students retrieved successfully');
    }

    /**
     * Store a newly created CourseStudent in storage.
     * POST /courseStudents
     *
     * @param CreateCourseStudentAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCourseStudentAPIRequest $request)
    {
        $input = $request->all();

        $courseStudents = $this->courseStudentRepository->create($input);

        return $this->sendResponse($courseStudents->toArray(), 'Course Student saved successfully');
    }

    /**
     * Display the specified CourseStudent.
     * GET|HEAD /courseStudents/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var CourseStudent $courseStudent */
        $courseStudent = $this->courseStudentRepository->findWithoutFail($id);

        if (empty($courseStudent)) {
            return $this->sendError('Course Student not found');
        }

        return $this->sendResponse($courseStudent->toArray(), 'Course Student retrieved successfully');
    }

    /**
     * Update the specified CourseStudent in storage.
     * PUT/PATCH /courseStudents/{id}
     *
     * @param  int $id
     * @param UpdateCourseStudentAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCourseStudentAPIRequest $request)
    {
        $input = $request->all();

        /** @var CourseStudent $courseStudent */
        $courseStudent = $this->courseStudentRepository->findWithoutFail($id);

        if (empty($courseStudent)) {
            return $this->sendError('Course Student not found');
        }

        $courseStudent = $this->courseStudentRepository->update($input, $id);

        return $this->sendResponse($courseStudent->toArray(), 'CourseStudent updated successfully');
    }

    /**
     * Remove the specified CourseStudent from storage.
     * DELETE /courseStudents/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var CourseStudent $courseStudent */
        $courseStudent = $this->courseStudentRepository->findWithoutFail($id);

        if (empty($courseStudent)) {
            return $this->sendError('Course Student not found');
        }

        $courseStudent->delete();

        return $this->sendResponse($id, 'Course Student deleted successfully');
    }
}
