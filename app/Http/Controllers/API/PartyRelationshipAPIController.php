<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePartyRelationshipAPIRequest;
use App\Http\Requests\API\UpdatePartyRelationshipAPIRequest;
use App\Models\PartyRelationship;
use App\Repositories\PartyRelationshipRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PartyRelationshipController
 * @package App\Http\Controllers\API
 */

class PartyRelationshipAPIController extends AppBaseController
{
    /** @var  PartyRelationshipRepository */
    private $partyRelationshipRepository;

    public function __construct(PartyRelationshipRepository $partyRelationshipRepo)
    {
        $this->partyRelationshipRepository = $partyRelationshipRepo;
    }

    /**
     * Display a listing of the PartyRelationship.
     * GET|HEAD /partyRelationships
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->partyRelationshipRepository->pushCriteria(new RequestCriteria($request));
        $this->partyRelationshipRepository->pushCriteria(new LimitOffsetCriteria($request));
        $partyRelationships = $this->partyRelationshipRepository->all();

        return $this->sendResponse($partyRelationships->toArray(), 'Party Relationships retrieved successfully');
    }

    /**
     * Store a newly created PartyRelationship in storage.
     * POST /partyRelationships
     *
     * @param CreatePartyRelationshipAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePartyRelationshipAPIRequest $request)
    {
        $input = $request->all();

        $partyRelationships = $this->partyRelationshipRepository->create($input);

        return $this->sendResponse($partyRelationships->toArray(), 'Party Relationship saved successfully');
    }

    /**
     * Display the specified PartyRelationship.
     * GET|HEAD /partyRelationships/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var PartyRelationship $partyRelationship */
        $partyRelationship = $this->partyRelationshipRepository->findWithoutFail($id);

        if (empty($partyRelationship)) {
            return $this->sendError('Party Relationship not found');
        }

        return $this->sendResponse($partyRelationship->toArray(), 'Party Relationship retrieved successfully');
    }

    /**
     * Update the specified PartyRelationship in storage.
     * PUT/PATCH /partyRelationships/{id}
     *
     * @param  int $id
     * @param UpdatePartyRelationshipAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePartyRelationshipAPIRequest $request)
    {
        $input = $request->all();

        /** @var PartyRelationship $partyRelationship */
        $partyRelationship = $this->partyRelationshipRepository->findWithoutFail($id);

        if (empty($partyRelationship)) {
            return $this->sendError('Party Relationship not found');
        }

        $partyRelationship = $this->partyRelationshipRepository->update($input, $id);

        return $this->sendResponse($partyRelationship->toArray(), 'PartyRelationship updated successfully');
    }

    /**
     * Remove the specified PartyRelationship from storage.
     * DELETE /partyRelationships/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var PartyRelationship $partyRelationship */
        $partyRelationship = $this->partyRelationshipRepository->findWithoutFail($id);

        if (empty($partyRelationship)) {
            return $this->sendError('Party Relationship not found');
        }

        $partyRelationship->delete();

        return $this->sendResponse($id, 'Party Relationship deleted successfully');
    }
}
