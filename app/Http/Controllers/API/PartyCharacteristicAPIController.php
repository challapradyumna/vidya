<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePartyCharacteristicAPIRequest;
use App\Http\Requests\API\UpdatePartyCharacteristicAPIRequest;
use App\Models\PartyCharacteristic;
use App\Repositories\PartyCharacteristicRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PartyCharacteristicController
 * @package App\Http\Controllers\API
 */

class PartyCharacteristicAPIController extends AppBaseController
{
    /** @var  PartyCharacteristicRepository */
    private $partyCharacteristicRepository;

    public function __construct(PartyCharacteristicRepository $partyCharacteristicRepo)
    {
        $this->partyCharacteristicRepository = $partyCharacteristicRepo;
    }

    /**
     * Display a listing of the PartyCharacteristic.
     * GET|HEAD /partyCharacteristics
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->partyCharacteristicRepository->pushCriteria(new RequestCriteria($request));
        $this->partyCharacteristicRepository->pushCriteria(new LimitOffsetCriteria($request));
        $partyCharacteristics = $this->partyCharacteristicRepository->all();

        return $this->sendResponse($partyCharacteristics->toArray(), 'Party Characteristics retrieved successfully');
    }

    /**
     * Store a newly created PartyCharacteristic in storage.
     * POST /partyCharacteristics
     *
     * @param CreatePartyCharacteristicAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePartyCharacteristicAPIRequest $request)
    {
        $input = $request->all();

        $partyCharacteristics = $this->partyCharacteristicRepository->create($input);

        return $this->sendResponse($partyCharacteristics->toArray(), 'Party Characteristic saved successfully');
    }

    /**
     * Display the specified PartyCharacteristic.
     * GET|HEAD /partyCharacteristics/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var PartyCharacteristic $partyCharacteristic */
        $partyCharacteristic = $this->partyCharacteristicRepository->findWithoutFail($id);

        if (empty($partyCharacteristic)) {
            return $this->sendError('Party Characteristic not found');
        }

        return $this->sendResponse($partyCharacteristic->toArray(), 'Party Characteristic retrieved successfully');
    }

    /**
     * Update the specified PartyCharacteristic in storage.
     * PUT/PATCH /partyCharacteristics/{id}
     *
     * @param  int $id
     * @param UpdatePartyCharacteristicAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePartyCharacteristicAPIRequest $request)
    {
        $input = $request->all();

        /** @var PartyCharacteristic $partyCharacteristic */
        $partyCharacteristic = $this->partyCharacteristicRepository->findWithoutFail($id);

        if (empty($partyCharacteristic)) {
            return $this->sendError('Party Characteristic not found');
        }

        $partyCharacteristic = $this->partyCharacteristicRepository->update($input, $id);

        return $this->sendResponse($partyCharacteristic->toArray(), 'PartyCharacteristic updated successfully');
    }

    /**
     * Remove the specified PartyCharacteristic from storage.
     * DELETE /partyCharacteristics/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var PartyCharacteristic $partyCharacteristic */
        $partyCharacteristic = $this->partyCharacteristicRepository->findWithoutFail($id);

        if (empty($partyCharacteristic)) {
            return $this->sendError('Party Characteristic not found');
        }

        $partyCharacteristic->delete();

        return $this->sendResponse($id, 'Party Characteristic deleted successfully');
    }
}
