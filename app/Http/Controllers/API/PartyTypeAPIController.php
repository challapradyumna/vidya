<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePartyTypeAPIRequest;
use App\Http\Requests\API\UpdatePartyTypeAPIRequest;
use App\Models\PartyType;
use App\Repositories\PartyTypeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as Controller;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PartyTypeController
 * @package App\Http\Controllers\API
 */

class PartyTypeAPIController extends Controller
{
    /** @var  PartyTypeRepository */
    private $partyTypeRepository;

    public function __construct(PartyTypeRepository $partyTypeRepo)
    {
        $this->partyTypeRepository = $partyTypeRepo;
    }

    /**
     * Display a listing of the PartyType.
     * GET|HEAD /partyTypes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->partyTypeRepository->pushCriteria(new RequestCriteria($request));
        $this->partyTypeRepository->pushCriteria(new LimitOffsetCriteria($request));
        $partyTypes = $this->partyTypeRepository->paginate(10);
        return $this->sendResponse($partyTypes->toArray(), 'Party Types retrieved successfully');
    }

    /**
     * Store a newly created PartyType in storage.
     * POST /partyTypes
     *
     * @param CreatePartyTypeAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePartyTypeAPIRequest $request)
    {
        $input = $request->all();

        $partyTypes = $this->partyTypeRepository->create($input);

        return $this->sendResponse($partyTypes->toArray(), 'Party Type saved successfully');
    }

    /**
     * Display the specified PartyType.
     * GET|HEAD /partyTypes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var PartyType $partyType */
        $partyType = $this->partyTypeRepository->findWithoutFail($id);

        if (empty($partyType)) {
            return $this->sendError('Party Type not found');
        }
        return $this->sendResponse($partyType->toArray(), 'Party Type retrieved successfully');
    }

    /**
     * Update the specified PartyType in storage.
     * PUT/PATCH /partyTypes/{id}
     *
     * @param  int $id
     * @param UpdatePartyTypeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePartyTypeAPIRequest $request)
    {
        $input = $request->all();

        /** @var PartyType $partyType */
        $partyType = $this->partyTypeRepository->findWithoutFail($id);

        if (empty($partyType)) {
            return $this->sendError('Party Type not found');
        }

        $partyType = $this->partyTypeRepository->update($input, $id);

        return $this->sendResponse($partyType->toArray(), 'PartyType updated successfully');
    }

    /**
     * Remove the specified PartyType from storage.
     * DELETE /partyTypes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var PartyType $partyType */
        $partyType = $this->partyTypeRepository->findWithoutFail($id);

        if (empty($partyType)) {
            return $this->sendError('Party Type not found');
        }

        $partyType->delete();

        return $this->sendResponse($id, 'Party Type deleted successfully');
    }
}
