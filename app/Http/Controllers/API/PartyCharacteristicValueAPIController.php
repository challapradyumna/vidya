<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePartyCharacteristicValueAPIRequest;
use App\Http\Requests\API\UpdatePartyCharacteristicValueAPIRequest;
use App\Models\PartyCharacteristicValue;
use App\Repositories\PartyCharacteristicValueRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PartyCharacteristicValueController
 * @package App\Http\Controllers\API
 */

class PartyCharacteristicValueAPIController extends AppBaseController
{
    /** @var  PartyCharacteristicValueRepository */
    private $partyCharacteristicValueRepository;

    public function __construct(PartyCharacteristicValueRepository $partyCharacteristicValueRepo)
    {
        $this->partyCharacteristicValueRepository = $partyCharacteristicValueRepo;
    }

    /**
     * Display a listing of the PartyCharacteristicValue.
     * GET|HEAD /partyCharacteristicValues
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->partyCharacteristicValueRepository->pushCriteria(new RequestCriteria($request));
        $this->partyCharacteristicValueRepository->pushCriteria(new LimitOffsetCriteria($request));
        $partyCharacteristicValues = $this->partyCharacteristicValueRepository->all();

        return $this->sendResponse($partyCharacteristicValues->toArray(), 'Party Characteristic Values retrieved successfully');
    }

    /**
     * Store a newly created PartyCharacteristicValue in storage.
     * POST /partyCharacteristicValues
     *
     * @param CreatePartyCharacteristicValueAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePartyCharacteristicValueAPIRequest $request)
    {
        $input = $request->all();

        $partyCharacteristicValues = $this->partyCharacteristicValueRepository->create($input);

        return $this->sendResponse($partyCharacteristicValues->toArray(), 'Party Characteristic Value saved successfully');
    }

    /**
     * Display the specified PartyCharacteristicValue.
     * GET|HEAD /partyCharacteristicValues/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var PartyCharacteristicValue $partyCharacteristicValue */
        $partyCharacteristicValue = $this->partyCharacteristicValueRepository->findWithoutFail($id);

        if (empty($partyCharacteristicValue)) {
            return $this->sendError('Party Characteristic Value not found');
        }

        return $this->sendResponse($partyCharacteristicValue->toArray(), 'Party Characteristic Value retrieved successfully');
    }

    /**
     * Update the specified PartyCharacteristicValue in storage.
     * PUT/PATCH /partyCharacteristicValues/{id}
     *
     * @param  int $id
     * @param UpdatePartyCharacteristicValueAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePartyCharacteristicValueAPIRequest $request)
    {
        $input = $request->all();

        /** @var PartyCharacteristicValue $partyCharacteristicValue */
        $partyCharacteristicValue = $this->partyCharacteristicValueRepository->findWithoutFail($id);

        if (empty($partyCharacteristicValue)) {
            return $this->sendError('Party Characteristic Value not found');
        }

        $partyCharacteristicValue = $this->partyCharacteristicValueRepository->update($input, $id);

        return $this->sendResponse($partyCharacteristicValue->toArray(), 'PartyCharacteristicValue updated successfully');
    }

    /**
     * Remove the specified PartyCharacteristicValue from storage.
     * DELETE /partyCharacteristicValues/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var PartyCharacteristicValue $partyCharacteristicValue */
        $partyCharacteristicValue = $this->partyCharacteristicValueRepository->findWithoutFail($id);

        if (empty($partyCharacteristicValue)) {
            return $this->sendError('Party Characteristic Value not found');
        }

        $partyCharacteristicValue->delete();

        return $this->sendResponse($id, 'Party Characteristic Value deleted successfully');
    }
}
