<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePartyIdentifierAPIRequest;
use App\Http\Requests\API\UpdatePartyIdentifierAPIRequest;
use App\Models\PartyIdentifier;
use App\Repositories\PartyIdentifierRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PartyIdentifierController
 * @package App\Http\Controllers\API
 */

class PartyIdentifierAPIController extends AppBaseController
{
    /** @var  PartyIdentifierRepository */
    private $partyIdentifierRepository;

    public function __construct(PartyIdentifierRepository $partyIdentifierRepo)
    {
        $this->partyIdentifierRepository = $partyIdentifierRepo;
    }

    /**
     * Display a listing of the PartyIdentifier.
     * GET|HEAD /partyIdentifiers
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->partyIdentifierRepository->pushCriteria(new RequestCriteria($request));
        $this->partyIdentifierRepository->pushCriteria(new LimitOffsetCriteria($request));
        $partyIdentifiers = $this->partyIdentifierRepository->paginate(10);

        return $this->sendResponse($partyIdentifiers->toArray(), 'Party Identifiers retrieved successfully');
    }

    /**
     * Store a newly created PartyIdentifier in storage.
     * POST /partyIdentifiers
     *
     * @param CreatePartyIdentifierAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePartyIdentifierAPIRequest $request)
    {
        $input = $request->all();

        $partyIdentifiers = $this->partyIdentifierRepository->create($input);

        return $this->sendResponse($partyIdentifiers->toArray(), 'Party Identifier saved successfully');
    }

    /**
     * Display the specified PartyIdentifier.
     * GET|HEAD /partyIdentifiers/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var PartyIdentifier $partyIdentifier */
        $partyIdentifier = $this->partyIdentifierRepository->findWithoutFail($id);

        if (empty($partyIdentifier)) {
            return $this->sendError('Party Identifier not found');
        }

        return $this->sendResponse($partyIdentifier->toArray(), 'Party Identifier retrieved successfully');
    }

    /**
     * Update the specified PartyIdentifier in storage.
     * PUT/PATCH /partyIdentifiers/{id}
     *
     * @param  int $id
     * @param UpdatePartyIdentifierAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePartyIdentifierAPIRequest $request)
    {
        $input = $request->all();

        /** @var PartyIdentifier $partyIdentifier */
        $partyIdentifier = $this->partyIdentifierRepository->findWithoutFail($id);

        if (empty($partyIdentifier)) {
            return $this->sendError('Party Identifier not found');
        }

        $partyIdentifier = $this->partyIdentifierRepository->update($input, $id);

        return $this->sendResponse($partyIdentifier->toArray(), 'PartyIdentifier updated successfully');
    }

    /**
     * Remove the specified PartyIdentifier from storage.
     * DELETE /partyIdentifiers/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var PartyIdentifier $partyIdentifier */
        $partyIdentifier = $this->partyIdentifierRepository->findWithoutFail($id);

        if (empty($partyIdentifier)) {
            return $this->sendError('Party Identifier not found');
        }

        $partyIdentifier->delete();

        return $this->sendResponse($id, 'Party Identifier deleted successfully');
    }
}
