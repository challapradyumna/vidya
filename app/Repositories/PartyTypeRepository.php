<?php

namespace App\Repositories;

use App\Models\PartyType;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PartyTypeRepository
 * @package App\Repositories
 * @version May 16, 2018, 4:13 pm UTC
 *
 * @method PartyType findWithoutFail($id, $columns = ['*'])
 * @method PartyType find($id, $columns = ['*'])
 * @method PartyType first($columns = ['*'])
*/
class PartyTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'=>'like'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PartyType::class;
    }
}
