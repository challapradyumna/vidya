<?php

namespace App\Repositories;

use App\Models\CourseStudent;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class CourseStudentRepository
 * @package App\Repositories
 * @version May 27, 2018, 5:20 am UTC
 *
 * @method CourseStudent findWithoutFail($id, $columns = ['*'])
 * @method CourseStudent find($id, $columns = ['*'])
 * @method CourseStudent first($columns = ['*'])
*/
class CourseStudentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'course_id',
        'student_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CourseStudent::class;
    }
}
