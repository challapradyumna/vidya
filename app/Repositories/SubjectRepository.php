<?php

namespace App\Repositories;

use App\Models\Subject;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class SubjectRepository
 * @package App\Repositories
 * @version May 27, 2018, 5:20 am UTC
 *
 * @method Subject findWithoutFail($id, $columns = ['*'])
 * @method Subject find($id, $columns = ['*'])
 * @method Subject first($columns = ['*'])
*/
class SubjectRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'course_id',
        'subject',
        'taught_by'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Subject::class;
    }
}
