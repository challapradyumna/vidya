<?php

namespace App\Repositories;

use App\Models\PartyCharacteristicAssignment;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PartyCharacteristicAssignmentRepository
 * @package App\Repositories
 * @version May 27, 2018, 5:18 am UTC
 *
 * @method PartyCharacteristicAssignment findWithoutFail($id, $columns = ['*'])
 * @method PartyCharacteristicAssignment find($id, $columns = ['*'])
 * @method PartyCharacteristicAssignment first($columns = ['*'])
*/
class PartyCharacteristicAssignmentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'party_type_id',
        'party_characteristic_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PartyCharacteristicAssignment::class;
    }
}
