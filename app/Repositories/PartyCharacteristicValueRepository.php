<?php

namespace App\Repositories;

use App\Models\PartyCharacteristicValue;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PartyCharacteristicValueRepository
 * @package App\Repositories
 * @version May 27, 2018, 5:17 am UTC
 *
 * @method PartyCharacteristicValue findWithoutFail($id, $columns = ['*'])
 * @method PartyCharacteristicValue find($id, $columns = ['*'])
 * @method PartyCharacteristicValue first($columns = ['*'])
*/
class PartyCharacteristicValueRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'value',
        'begin',
        'end',
        'by_party_id',
        'characteristic_type_id',
        'to_party_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PartyCharacteristicValue::class;
    }
}
