<?php

namespace App\Repositories;

use App\Models\PartyIdentifier;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PartyIdentifierRepository
 * @package App\Repositories
 * @version May 27, 2018, 5:16 am UTC
 *
 * @method PartyIdentifier findWithoutFail($id, $columns = ['*'])
 * @method PartyIdentifier find($id, $columns = ['*'])
 * @method PartyIdentifier first($columns = ['*'])
*/
class PartyIdentifierRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description',
        'issued_by'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PartyIdentifier::class;
    }
}
