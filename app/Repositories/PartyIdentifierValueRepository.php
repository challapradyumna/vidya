<?php

namespace App\Repositories;

use App\Models\PartyIdentifierValue;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PartyIdentifierValueRepository
 * @package App\Repositories
 * @version May 27, 2018, 5:14 am UTC
 *
 * @method PartyIdentifierValue findWithoutFail($id, $columns = ['*'])
 * @method PartyIdentifierValue find($id, $columns = ['*'])
 * @method PartyIdentifierValue first($columns = ['*'])
*/
class PartyIdentifierValueRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'value',
        'begin',
        'end',
        'comment',
        'from',
        'to',
        'party_identifier_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PartyIdentifierValue::class;
    }
}
