<?php

namespace App\Repositories;

use App\Models\PartyCharacteristic;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PartyCharacteristicRepository
 * @package App\Repositories
 * @version May 27, 2018, 5:18 am UTC
 *
 * @method PartyCharacteristic findWithoutFail($id, $columns = ['*'])
 * @method PartyCharacteristic find($id, $columns = ['*'])
 * @method PartyCharacteristic first($columns = ['*'])
*/
class PartyCharacteristicRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description',
        'default_value',
        'defined_by'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PartyCharacteristic::class;
    }
}
