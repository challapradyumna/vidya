<?php

namespace App\Repositories;

use App\Models\PartyRelationship;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PartyRelationshipRepository
 * @package App\Repositories
 * @version May 27, 2018, 5:18 am UTC
 *
 * @method PartyRelationship findWithoutFail($id, $columns = ['*'])
 * @method PartyRelationship find($id, $columns = ['*'])
 * @method PartyRelationship first($columns = ['*'])
*/
class PartyRelationshipRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'begin',
        'end',
        'from',
        'to',
        'party_relationship_type_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PartyRelationship::class;
    }
}
