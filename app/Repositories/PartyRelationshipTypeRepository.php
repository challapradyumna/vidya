<?php

namespace App\Repositories;

use App\Models\PartyRelationshipType;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PartyRelationshipTypeRepository
 * @package App\Repositories
 * @version May 27, 2018, 5:19 am UTC
 *
 * @method PartyRelationshipType findWithoutFail($id, $columns = ['*'])
 * @method PartyRelationshipType find($id, $columns = ['*'])
 * @method PartyRelationshipType first($columns = ['*'])
*/
class PartyRelationshipTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PartyRelationshipType::class;
    }
}
