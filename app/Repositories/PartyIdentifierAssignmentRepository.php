<?php

namespace App\Repositories;

use App\Models\PartyIdentifierAssignment;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PartyIdentifierAssignmentRepository
 * @package App\Repositories
 * @version May 27, 2018, 5:17 am UTC
 *
 * @method PartyIdentifierAssignment findWithoutFail($id, $columns = ['*'])
 * @method PartyIdentifierAssignment find($id, $columns = ['*'])
 * @method PartyIdentifierAssignment first($columns = ['*'])
*/
class PartyIdentifierAssignmentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'party_type_id',
        'party_identifier_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PartyIdentifierAssignment::class;
    }
}
