<?php

namespace App\Repositories;

use App\Models\StudentSubject;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class StudentSubjectRepository
 * @package App\Repositories
 * @version May 27, 2018, 5:21 am UTC
 *
 * @method StudentSubject findWithoutFail($id, $columns = ['*'])
 * @method StudentSubject find($id, $columns = ['*'])
 * @method StudentSubject first($columns = ['*'])
*/
class StudentSubjectRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'student_id',
        'subject_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return StudentSubject::class;
    }
}
