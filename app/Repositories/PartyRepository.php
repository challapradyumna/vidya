<?php

namespace App\Repositories;

use App\Models\Party;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PartyRepository
 * @package App\Repositories
 * @version May 10, 2018, 3:38 pm UTC
 *
 * @method Party findWithoutFail($id, $columns = ['*'])
 * @method Party find($id, $columns = ['*'])
 * @method Party first($columns = ['*'])
*/
class PartyRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'=>'like',
        'party_type_id',
        'user_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Party::class;
    }
}
