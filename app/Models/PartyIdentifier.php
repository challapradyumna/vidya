<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PartyIdentifier
 * @package App\Models
 * @version May 27, 2018, 5:16 am UTC
 *
 * @property \App\Models\Party party
 * @property \Illuminate\Database\Eloquent\Collection courseStudent
 * @property \Illuminate\Database\Eloquent\Collection party
 * @property \Illuminate\Database\Eloquent\Collection partyCharacteristicAssignment
 * @property \Illuminate\Database\Eloquent\Collection PartyIdentifierAssignment
 * @property \Illuminate\Database\Eloquent\Collection PartyIdentifierValue
 * @property \Illuminate\Database\Eloquent\Collection studentSubject
 * @property \Illuminate\Database\Eloquent\Collection subject
 * @property string name
 * @property string description
 * @property integer issued_by
 */
class PartyIdentifier extends Model
{
    use SoftDeletes;

    public $table = 'party_identifier';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'description',
        'issued_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'description' => 'string',
        'issued_by' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function issuedBy()
    {
        return $this->belongsTo(\App\Models\Party::class,'issued_by','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function partyIdentifierAssignments()
    {
        return $this->hasMany(\App\Models\PartyIdentifierAssignment::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function partyIdentifierValues()
    {
        return $this->hasMany(\App\Models\PartyIdentifierValue::class);
    }
}
