<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PartyRelationshipType
 * @package App\Models
 * @version May 27, 2018, 5:19 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection courseStudent
 * @property \Illuminate\Database\Eloquent\Collection party
 * @property \Illuminate\Database\Eloquent\Collection partyCharacteristicAssignment
 * @property \Illuminate\Database\Eloquent\Collection partyIdentifierAssignment
 * @property \Illuminate\Database\Eloquent\Collection PartyRelationship
 * @property \Illuminate\Database\Eloquent\Collection studentSubject
 * @property \Illuminate\Database\Eloquent\Collection subject
 * @property string name
 * @property string description
 */
class PartyRelationshipType extends Model
{
    use SoftDeletes;

    public $table = 'party_relationship_type';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function partyRelationships()
    {
        return $this->hasMany(\App\Models\PartyRelationship::class);
    }
}
