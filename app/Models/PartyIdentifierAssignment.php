<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PartyIdentifierAssignment
 * @package App\Models
 * @version May 27, 2018, 5:17 am UTC
 *
 * @property \App\Models\PartyIdentifier partyIdentifier
 * @property \App\Models\PartyType partyType
 * @property \Illuminate\Database\Eloquent\Collection courseStudent
 * @property \Illuminate\Database\Eloquent\Collection party
 * @property \Illuminate\Database\Eloquent\Collection partyCharacteristicAssignment
 * @property \Illuminate\Database\Eloquent\Collection studentSubject
 * @property \Illuminate\Database\Eloquent\Collection subject
 * @property integer party_type_id
 * @property integer party_identifier_id
 */
class PartyIdentifierAssignment extends Model
{
    use SoftDeletes;

    public $table = 'party_identifier_assignment';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'party_type_id',
        'party_identifier_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'party_type_id' => 'integer',
        'party_identifier_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function partyIdentifier()
    {
        return $this->belongsTo(\App\Models\PartyIdentifier::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function partyType()
    {
        return $this->belongsTo(\App\Models\PartyType::class);
    }
}
