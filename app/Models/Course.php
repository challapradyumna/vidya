<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Course
 * @package App\Models
 * @version May 27, 2018, 5:20 am UTC
 *
 * @property \App\Models\Party party
 * @property \Illuminate\Database\Eloquent\Collection CourseStudent
 * @property \Illuminate\Database\Eloquent\Collection party
 * @property \Illuminate\Database\Eloquent\Collection partyCharacteristicAssignment
 * @property \Illuminate\Database\Eloquent\Collection partyIdentifierAssignment
 * @property \Illuminate\Database\Eloquent\Collection studentSubject
 * @property \Illuminate\Database\Eloquent\Collection Subject
 * @property integer offered_by
 * @property string subject
 * @property string year
 * @property string section
 */
class Course extends Model
{
    use SoftDeletes;

    public $table = 'course';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'offered_by',
        'subject',
        'year',
        'section'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'offered_by' => 'integer',
        'subject' => 'string',
        'year' => 'string',
        'section' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function party()
    {
        return $this->belongsTo(\App\Models\Party::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function courseStudents()
    {
        return $this->hasMany(\App\Models\CourseStudent::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function subjects()
    {
        return $this->hasMany(\App\Models\Subject::class);
    }
}
