<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PartyType
 * @package App\Models
 * @version May 16, 2018, 4:13 pm UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection courseStudent
 * @property \Illuminate\Database\Eloquent\Collection Party
 * @property \Illuminate\Database\Eloquent\Collection PartyCharacteristicAssignment
 * @property \Illuminate\Database\Eloquent\Collection PartyIdentifierAssignment
 * @property \Illuminate\Database\Eloquent\Collection studentSubject
 * @property \Illuminate\Database\Eloquent\Collection subject
 * @property string name
 */
class PartyType extends Model
{
    use SoftDeletes;

    public $table = 'party_type';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'parent_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'parent_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function parties()
    {
        return $this->hasMany(\App\Models\Party::class);
    }

    public function partyType()
    {
        return $this->hasOne(\App\Models\PartyType::class,'id','parent_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function partyCharacteristicAssignments()
    {
        return $this->hasMany(\App\Models\PartyCharacteristicAssignment::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function partyIdentifierAssignments()
    {
        return $this->hasMany(\App\Models\PartyIdentifierAssignment::class);
    }
}
