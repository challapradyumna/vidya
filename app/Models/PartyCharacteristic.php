<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PartyCharacteristic
 * @package App\Models
 * @version May 27, 2018, 5:18 am UTC
 *
 * @property \App\Models\Party party
 * @property \Illuminate\Database\Eloquent\Collection courseStudent
 * @property \Illuminate\Database\Eloquent\Collection party
 * @property \Illuminate\Database\Eloquent\Collection PartyCharacteristicAssignment
 * @property \Illuminate\Database\Eloquent\Collection PartyCharacteristicValue
 * @property \Illuminate\Database\Eloquent\Collection partyIdentifierAssignment
 * @property \Illuminate\Database\Eloquent\Collection studentSubject
 * @property \Illuminate\Database\Eloquent\Collection subject
 * @property string name
 * @property string description
 * @property string default_value
 * @property integer defined_by
 */
class PartyCharacteristic extends Model
{
    use SoftDeletes;

    public $table = 'party_characteristic';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'description',
        'default_value',
        'defined_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'description' => 'string',
        'default_value' => 'string',
        'defined_by' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function party()
    {
        return $this->belongsTo(\App\Models\Party::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function partyCharacteristicAssignments()
    {
        return $this->hasMany(\App\Models\PartyCharacteristicAssignment::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function partyCharacteristicValues()
    {
        return $this->hasMany(\App\Models\PartyCharacteristicValue::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function party()
    {
        return $this->belongsTo(\App\Models\Party::class);
    }
}
