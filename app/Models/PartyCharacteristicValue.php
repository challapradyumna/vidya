<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PartyCharacteristicValue
 * @package App\Models
 * @version May 27, 2018, 5:17 am UTC
 *
 * @property \App\Models\Party party
 * @property \App\Models\PartyCharacteristic partyCharacteristic
 * @property \App\Models\Party party
 * @property \Illuminate\Database\Eloquent\Collection courseStudent
 * @property \Illuminate\Database\Eloquent\Collection party
 * @property \Illuminate\Database\Eloquent\Collection partyCharacteristicAssignment
 * @property \Illuminate\Database\Eloquent\Collection partyIdentifierAssignment
 * @property \Illuminate\Database\Eloquent\Collection studentSubject
 * @property \Illuminate\Database\Eloquent\Collection subject
 * @property string value
 * @property string|\Carbon\Carbon begin
 * @property string|\Carbon\Carbon end
 * @property integer by_party_id
 * @property integer characteristic_type_id
 * @property integer to_party_id
 */
class PartyCharacteristicValue extends Model
{
    use SoftDeletes;

    public $table = 'party_characteristic_value';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'value',
        'begin',
        'end',
        'by_party_id',
        'characteristic_type_id',
        'to_party_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'value' => 'string',
        'by_party_id' => 'integer',
        'characteristic_type_id' => 'integer',
        'to_party_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function party()
    {
        return $this->belongsTo(\App\Models\Party::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function partyCharacteristic()
    {
        return $this->belongsTo(\App\Models\PartyCharacteristic::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function party()
    {
        return $this->belongsTo(\App\Models\Party::class);
    }
}
