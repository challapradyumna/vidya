<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PartyCharacteristicAssignment
 * @package App\Models
 * @version May 27, 2018, 5:18 am UTC
 *
 * @property \App\Models\PartyCharacteristic partyCharacteristic
 * @property \App\Models\PartyType partyType
 * @property \Illuminate\Database\Eloquent\Collection courseStudent
 * @property \Illuminate\Database\Eloquent\Collection party
 * @property \Illuminate\Database\Eloquent\Collection partyIdentifierAssignment
 * @property \Illuminate\Database\Eloquent\Collection studentSubject
 * @property \Illuminate\Database\Eloquent\Collection subject
 * @property integer party_type_id
 * @property integer party_characteristic_id
 */
class PartyCharacteristicAssignment extends Model
{
    use SoftDeletes;

    public $table = 'party_characteristic_assignment';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'party_type_id',
        'party_characteristic_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'party_type_id' => 'integer',
        'party_characteristic_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function partyCharacteristic()
    {
        return $this->belongsTo(\App\Models\PartyCharacteristic::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function partyType()
    {
        return $this->belongsTo(\App\Models\PartyType::class);
    }
}
