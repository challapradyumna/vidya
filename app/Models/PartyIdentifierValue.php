<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PartyIdentifierValue
 * @package App\Models
 * @version May 27, 2018, 5:14 am UTC
 *
 * @property \App\Models\Party party
 * @property \App\Models\PartyIdentifier partyIdentifier
 * @property \App\Models\Party party
 * @property \Illuminate\Database\Eloquent\Collection courseStudent
 * @property \Illuminate\Database\Eloquent\Collection party
 * @property \Illuminate\Database\Eloquent\Collection partyCharacteristicAssignment
 * @property \Illuminate\Database\Eloquent\Collection partyIdentifierAssignment
 * @property \Illuminate\Database\Eloquent\Collection studentSubject
 * @property \Illuminate\Database\Eloquent\Collection subject
 * @property string value
 * @property string|\Carbon\Carbon begin
 * @property string|\Carbon\Carbon end
 * @property string comment
 * @property integer from
 * @property integer to
 * @property integer party_identifier_id
 */
class PartyIdentifierValue extends Model
{
    use SoftDeletes;

    public $table = 'party_identifier_value';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'value',
        'begin',
        'end',
        'comment',
        'from',
        'to',
        'party_identifier_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'value' => 'string',
        'comment' => 'string',
        'from' => 'integer',
        'to' => 'integer',
        'party_identifier_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

        /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function from()
    {
        return $this->belongsTo(\App\Models\Party::class,'from','id');
    }

    public function to(){
        return $this->belongsTo(\App\Models\Party::class,'to','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function partyIdentifier()
    {
        return $this->belongsTo(\App\Models\PartyIdentifier::class,'party_identifier_id','id');
    }


}
