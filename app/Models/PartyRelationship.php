<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PartyRelationship
 * @package App\Models
 * @version May 27, 2018, 5:18 am UTC
 *
 * @property \App\Models\Party party
 * @property \App\Models\PartyRelationshipType partyRelationshipType
 * @property \App\Models\Party party
 * @property \Illuminate\Database\Eloquent\Collection courseStudent
 * @property \Illuminate\Database\Eloquent\Collection party
 * @property \Illuminate\Database\Eloquent\Collection partyCharacteristicAssignment
 * @property \Illuminate\Database\Eloquent\Collection partyIdentifierAssignment
 * @property \Illuminate\Database\Eloquent\Collection studentSubject
 * @property \Illuminate\Database\Eloquent\Collection subject
 * @property string|\Carbon\Carbon begin
 * @property string|\Carbon\Carbon end
 * @property integer from
 * @property integer to
 * @property integer party_relationship_type_id
 */
class PartyRelationship extends Model
{
    use SoftDeletes;

    public $table = 'party_relationship';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'begin',
        'end',
        'from',
        'to',
        'party_relationship_type_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'from' => 'integer',
        'to' => 'integer',
        'party_relationship_type_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function party()
    {
        return $this->belongsTo(\App\Models\Party::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function partyRelationshipType()
    {
        return $this->belongsTo(\App\Models\PartyRelationshipType::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function party()
    {
        return $this->belongsTo(\App\Models\Party::class);
    }
}
