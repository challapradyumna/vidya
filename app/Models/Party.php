<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Party
 * @package App\Models
 * @version May 10, 2018, 3:38 pm UTC
 *
 * @property \App\Models\User user
 * @property \Illuminate\Database\Eloquent\Collection Course
 * @property \Illuminate\Database\Eloquent\Collection CourseStudent
 * @property \Illuminate\Database\Eloquent\Collection PartyCharacteristic
 * @property \Illuminate\Database\Eloquent\Collection partyCharacteristicAssignment
 * @property \Illuminate\Database\Eloquent\Collection PartyCharacteristicValue
 * @property \Illuminate\Database\Eloquent\Collection PartyCharacteristicValue
 * @property \Illuminate\Database\Eloquent\Collection PartyIdentifier
 * @property \Illuminate\Database\Eloquent\Collection partyIdentifierAssignment
 * @property \Illuminate\Database\Eloquent\Collection PartyIdentifierValue
 * @property \Illuminate\Database\Eloquent\Collection PartyIdentifierValue
 * @property \Illuminate\Database\Eloquent\Collection PartyRelationship
 * @property \Illuminate\Database\Eloquent\Collection PartyRelationship
 * @property \Illuminate\Database\Eloquent\Collection StudentSubject
 * @property \Illuminate\Database\Eloquent\Collection Subject
 * @property string name
 * @property integer party_type_id
 * @property integer user_id
 */
class Party extends Model
{
    use SoftDeletes;

    public $table = 'party';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'party_type_id',
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'party_type_id' => 'integer',
        'user_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    public function partyType()
    {
        return $this->hasOne(\App\Models\PartyType::class,'id','party_type_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function courses()
    {
        return $this->hasMany(\App\Models\Course::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function courseStudents()
    {
        return $this->hasMany(\App\Models\CourseStudent::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function partyCharacteristics()
    {
        return $this->hasMany(\App\Models\PartyCharacteristic::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function partyCharacteristicValues()
    {
        return $this->hasMany(\App\Models\PartyCharacteristicValue::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function partyIdentifiers()
    {
        return $this->hasMany(\App\Models\PartyIdentifier::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function partyIdentifierValues()
    {
        return $this->hasMany(\App\Models\PartyIdentifierValue::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function partyRelationships()
    {
        return $this->hasMany(\App\Models\PartyRelationship::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function studentSubjects()
    {
        return $this->hasMany(\App\Models\StudentSubject::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function subjects()
    {
        return $this->hasMany(\App\Models\Subject::class);
    }
}
