webpackJsonp([2,10],{

/***/ 158:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(288)
}
var normalizeComponent = __webpack_require__(4)
/* script */
var __vue_script__ = __webpack_require__(290)
/* template */
var __vue_template__ = __webpack_require__(291)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\js\\components\\party\\party_form.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-139259ec", Component.options)
  } else {
    hotAPI.reload("data-v-139259ec", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 262:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(270)
}
var normalizeComponent = __webpack_require__(4)
/* script */
var __vue_script__ = __webpack_require__(272)
/* template */
var __vue_template__ = __webpack_require__(273)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\js\\components\\form\\form.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-f21eecd6", Component.options)
  } else {
    hotAPI.reload("data-v-f21eecd6", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 267:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Model = function () {
    function Model() {
        _classCallCheck(this, Model);

        this.created_at = null;
        this.updated_at = null;
        this.deleted_at = null;
    }

    _createClass(Model, [{
        key: 'fillTableWith',
        value: function fillTableWith() {
            var _this = this;

            this.columns.forEach(function (col) {
                if (col.hasOwnProperty('relation')) {
                    _this.tableWith.push(col.relationWith);
                }
            });
        }
    }, {
        key: 'getPage',
        value: function getPage() {
            var page = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;
            var tableWith = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];

            return axios.get(this.url, {
                params: {
                    page: page,
                    with: this.tableWith.join(';')
                }
            });
        }
    }, {
        key: 'get',
        value: function get(id) {
            var _this2 = this;

            if (id) {
                return axios.get(this.url + "/" + id).then(function (response) {
                    _this2.columns.forEach(function (col) {
                        if (col.hasOwnProperty('relation')) {
                            if (col.relation === _this2) {
                                _this2.selfReference(col);
                            }
                            _this2[col.name].id = response.data[col.name];
                            _this2[col.name].get(response.data[col.name]);
                        } else {
                            _this2[col.name] = response.data[col.name];
                        }
                    });
                    return response;
                });
            }
        }
    }, {
        key: 'datatableSearch',
        value: function datatableSearch(term) {
            return axios.get(this.url, {
                params: {
                    search: term,
                    with: this.tableWith.join(',')
                }
            });
        }
    }, {
        key: 'search',
        value: function search(term) {
            var _this3 = this;

            return axios.get(this.url, {
                params: {
                    search: term
                }
            }).then(function (response) {
                return _this3.getSearchTerms(response.data.data);
            });
        }
    }, {
        key: 'getSearchTerms',
        value: function getSearchTerms(object) {
            var _this4 = this;

            var searchTerms = [];
            if (object == null) {
                return searchTerms;
            }
            object.forEach(function (row) {
                var Term = {};
                Term.name = '';
                Term.id = row.id;
                _this4.searchResultTerm.forEach(function (searchTerm) {
                    Term.name = Term.name + row[searchTerm] + " ";
                });
                searchTerms.push(Term);
            });
            return searchTerms;
        }
    }, {
        key: 'save',
        value: function save() {
            var _this5 = this;

            var postData = {};
            this.columns.forEach(function (col) {
                if (col.hasOwnProperty('relation')) {
                    postData[col.name] = _this5[col.name].id;
                } else {
                    if (col.type == 'datetime-local') {
                        postData[col.name] = moment(_this5[col.name]).format('YYYY-MM-DD HH:mm:ss');
                    } else {
                        postData[col.name] = _this5[col.name];
                    }
                }
            });
            var sendUrl = '';
            if (this.id != null) {
                sendUrl = this.url + '/' + this.id;
                return axios.put(sendUrl, postData).then(function (response) {
                    console.log(response);
                }).catch(function (error) {
                    console.log(error);
                });
            } else {
                sendUrl = this.url;
                return axios.post(sendUrl, postData).then(function (response) {
                    console.log(response);
                }).catch(function (error) {
                    console.log(error);
                });
            }
        }
    }]);

    return Model;
}();

/* harmony default export */ __webpack_exports__["a"] = (Model);

/***/ }),

/***/ 268:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__model__ = __webpack_require__(267);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_PartyType_js__ = __webpack_require__(268);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }




var PartyType = function (_Model) {
    _inherits(PartyType, _Model);

    function PartyType() {
        _classCallCheck(this, PartyType);

        var _this = _possibleConstructorReturn(this, (PartyType.__proto__ || Object.getPrototypeOf(PartyType)).call(this));

        _this.name = null;
        _this.id = null;
        _this.parent_id = _this;
        _this.url = '/api/party_types';
        _this.searchResultTerm = ['name'];
        _this.tableWith = [];
        _this.columns = [{
            name: 'name',
            type: 'text',
            label: 'Name',
            readOnly: false
        }, {
            name: 'id',
            type: 'number',
            label: 'Id',
            readOnly: true
        }, {
            name: 'parent_id',
            type: 'number',
            label: 'Parent',
            relationWith: 'partyType',
            relation: _this
        }];

        _this.fillTableWith();
        return _this;
    }

    _createClass(PartyType, [{
        key: 'selfReference',
        value: function selfReference() {
            var _this2 = this;

            var col = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

            if (col == null) {
                this.columns.forEach(function (col) {
                    if (col.hasOwnProperty('relation')) {
                        if (col.relation === _this2) {
                            _this2[col.name] = new PartyType();
                        }
                    }
                });
            } else {
                this[col.name] = new PartyType();
            }
        }
    }]);

    return PartyType;
}(__WEBPACK_IMPORTED_MODULE_0__model__["a" /* default */]);

/* harmony default export */ __webpack_exports__["a"] = (PartyType);

/***/ }),

/***/ 269:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__PartyType__ = __webpack_require__(268);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__model__ = __webpack_require__(267);
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }




var Party = function (_Model) {
    _inherits(Party, _Model);

    function Party() {
        _classCallCheck(this, Party);

        var _this = _possibleConstructorReturn(this, (Party.__proto__ || Object.getPrototypeOf(Party)).call(this));

        _this.id = null;
        _this.name = null;
        _this.party_type_id = new __WEBPACK_IMPORTED_MODULE_0__PartyType__["a" /* default */]();
        _this.searchResultTerm = ['name'];
        _this.url = '/api/parties';
        _this.tableWith = [];
        _this.columns = [{
            name: 'id',
            type: 'number',
            readOnly: true,
            label: 'ID'
        }, {
            name: 'name',
            type: 'text',
            label: 'Name'
        }, {
            name: 'party_type_id',
            type: 'number',
            label: 'Party Type',
            relationWith: 'partyType',
            relation: new __WEBPACK_IMPORTED_MODULE_0__PartyType__["a" /* default */]()
        }];

        _this.fillTableWith();
        return _this;
    }

    return Party;
}(__WEBPACK_IMPORTED_MODULE_1__model__["a" /* default */]);

/* harmony default export */ __webpack_exports__["a"] = (Party);

/***/ }),

/***/ 270:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(271);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(3)("c9bcd2dc", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-f21eecd6\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./form.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-f21eecd6\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./form.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 271:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(2)(false);
// imports


// module
exports.push([module.i, "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", ""]);

// exports


/***/ }),

/***/ 272:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var Form = function Form() {
    _classCallCheck(this, Form);
};

/* harmony default export */ __webpack_exports__["default"] = ({
    props: ['object'],
    data: function data() {
        return {
            sending: false,
            form: null,
            searchData: [],
            currentFormItem: null
        };
    },
    methods: {
        ifError: function ifError(column) {
            var colName = column.name;
            console.log(this.object.colName == null);
            return this.object.colName == null;
        },
        searchColumn: function searchColumn(column) {
            var _this = this;

            this.currentFormItem = column.name;
            this.searchData = [];
            column.relation.search(this.object[column.name].name).then(function (response) {
                _this.searchData = response;
            });
        },
        selectedRow: function selectedRow(item) {
            this.object[this.currentFormItem].name = item.name;
            this.object[this.currentFormItem].id = item.id;
        },
        validateObject: function validateObject() {
            this.object.save();
        }
    },
    created: function created() {
        if (this.$route.params.hasOwnProperty('id')) {
            this.object.get(this.$route.params.id);
        }
    }
});

/***/ }),

/***/ 273:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c(
      "form",
      {
        staticClass: "md-layout",
        attrs: { novalidate: "" },
        on: {
          submit: function($event) {
            $event.preventDefault()
            return _vm.validateObject($event)
          }
        }
      },
      [
        _c("div", { staticClass: "md-layout md-gutter" }, [
          _c(
            "div",
            { staticClass: "md-layout-item" },
            [
              _vm._l(_vm.object.columns, function(column) {
                return !column.readOnly
                  ? _c(
                      "div",
                      { key: column.name },
                      [
                        column.type == "text"
                          ? _c(
                              "md-field",
                              [
                                _c("label", { attrs: { for: "column.name" } }, [
                                  _vm._v(_vm._s(column.label))
                                ]),
                                _vm._v(" "),
                                _c("md-input", {
                                  attrs: {
                                    name: "column.name",
                                    id: "column.name",
                                    disabled: _vm.sending
                                  },
                                  model: {
                                    value: _vm.object[column.name],
                                    callback: function($$v) {
                                      _vm.$set(_vm.object, column.name, $$v)
                                    },
                                    expression: "object[column.name]"
                                  }
                                })
                              ],
                              1
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        column.hasOwnProperty("relation")
                          ? _c(
                              "md-autocomplete",
                              {
                                attrs: { "md-options": _vm.searchData },
                                on: {
                                  "md-changed": function($event) {
                                    _vm.searchColumn(column)
                                  },
                                  "md-opened": function($event) {
                                    _vm.searchColumn(column)
                                  },
                                  "md-selected": _vm.selectedRow
                                },
                                scopedSlots: _vm._u([
                                  {
                                    key: "md-autocomplete-item",
                                    fn: function(ref) {
                                      var item = ref.item
                                      return [_vm._v(_vm._s(item.name))]
                                    }
                                  }
                                ]),
                                model: {
                                  value: _vm.object[column.name].name,
                                  callback: function($$v) {
                                    _vm.$set(
                                      _vm.object[column.name],
                                      "name",
                                      $$v
                                    )
                                  },
                                  expression: "object[column.name].name"
                                }
                              },
                              [_c("label", [_vm._v(_vm._s(column.label))])]
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        column.type == "datetime-local"
                          ? _c(
                              "md-datepicker",
                              {
                                model: {
                                  value: _vm.object[column.name],
                                  callback: function($$v) {
                                    _vm.$set(_vm.object, column.name, $$v)
                                  },
                                  expression: "object[column.name]"
                                }
                              },
                              [_c("label", [_vm._v(_vm._s(column.label))])]
                            )
                          : _vm._e()
                      ],
                      1
                    )
                  : _vm._e()
              }),
              _vm._v(" "),
              _c(
                "md-button",
                {
                  staticClass: "md-raised md-primary",
                  on: {
                    click: function($event) {
                      _vm.object.save()
                    }
                  }
                },
                [_vm._v("Save")]
              ),
              _vm._v(" "),
              _c(
                "md-button",
                {
                  staticClass: "md-raised md-accent",
                  on: {
                    click: function($event) {
                      _vm.$router.go(-1)
                    }
                  }
                },
                [_vm._v("Cancel")]
              )
            ],
            2
          )
        ])
      ]
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-f21eecd6", module.exports)
  }
}

/***/ }),

/***/ 288:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(289);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(3)("5d7f1c4e", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-139259ec\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./party_form.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-139259ec\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./party_form.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 289:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(2)(false);
// imports


// module
exports.push([module.i, "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", ""]);

// exports


/***/ }),

/***/ 290:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__models_Party_js__ = __webpack_require__(269);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_form_form_vue__ = __webpack_require__(262);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_form_form_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__components_form_form_vue__);
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    'form-view': __WEBPACK_IMPORTED_MODULE_1__components_form_form_vue___default.a
  },
  data: function data() {
    return {
      party: new __WEBPACK_IMPORTED_MODULE_0__models_Party_js__["a" /* default */]()
    };
  },
  created: function created() {
    if (this.$route.params.hasOwnProperty('id')) {
      this.party.get(this.$route.params.id);
    }
  }
});

/***/ }),

/***/ 291:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [_c("form-view", { attrs: { object: this.party } })], 1)
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-139259ec", module.exports)
  }
}

/***/ })

});