webpackJsonp([4],{

/***/ 157:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(280)
}
var normalizeComponent = __webpack_require__(4)
/* script */
var __vue_script__ = __webpack_require__(282)
/* template */
var __vue_template__ = __webpack_require__(283)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\js\\components\\layouts\\app-header.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-fce2eefa", Component.options)
  } else {
    hotAPI.reload("data-v-fce2eefa", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 280:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(281);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(3)("0af58506", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-fce2eefa\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./app-header.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-fce2eefa\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./app-header.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 281:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(2)(false);
// imports


// module
exports.push([module.i, "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", ""]);

// exports


/***/ }),

/***/ 282:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'PersistentMini',
  data: function data() {
    return {
      menuVisible: false
    };
  },
  methods: {
    toggleMenu: function toggleMenu() {
      this.menuVisible = !this.menuVisible;
    }
  }
});

/***/ }),

/***/ 283:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "md-app-toolbar",
        { staticClass: "md-primary", attrs: { "md-elevation": "0" } },
        [
          !_vm.menuVisible
            ? _c(
                "md-button",
                {
                  staticClass: "md-icon-button",
                  on: { click: _vm.toggleMenu }
                },
                [_c("md-icon", [_vm._v("menu")])],
                1
              )
            : _vm._e(),
          _vm._v(" "),
          _c("span", { staticClass: "md-title" }, [_vm._v("My Title")])
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "md-app-drawer",
        {
          attrs: { "md-active": _vm.menuVisible, "md-persistent": "mini" },
          on: {
            "update:mdActive": function($event) {
              _vm.menuVisible = $event
            }
          }
        },
        [
          _c(
            "md-toolbar",
            { staticClass: "md-transparent", attrs: { "md-elevation": "0" } },
            [
              _c("span", [_vm._v("Navigation")]),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "md-toolbar-section-end" },
                [
                  _c(
                    "md-button",
                    {
                      staticClass: "md-icon-button md-dense",
                      on: { click: _vm.toggleMenu }
                    },
                    [_c("md-icon", [_vm._v("keyboard_arrow_left")])],
                    1
                  )
                ],
                1
              )
            ]
          ),
          _vm._v(" "),
          _c(
            "md-list",
            [
              _c(
                "md-list-item",
                [
                  _c("md-icon", [_vm._v("move_to_inbox")]),
                  _vm._v(" "),
                  _c("span", { staticClass: "md-list-item-text" }, [
                    _vm._v("Inbox")
                  ])
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "md-list-item",
                [
                  _c("md-icon", [_vm._v("send")]),
                  _vm._v(" "),
                  _c("span", { staticClass: "md-list-item-text" }, [
                    _vm._v("Sent Mail")
                  ])
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "md-list-item",
                [
                  _c("md-icon", [_vm._v("delete")]),
                  _vm._v(" "),
                  _c("span", { staticClass: "md-list-item-text" }, [
                    _vm._v("Trash")
                  ])
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "md-list-item",
                [
                  _c("md-icon", [_vm._v("error")]),
                  _vm._v(" "),
                  _c("span", { staticClass: "md-list-item-text" }, [
                    _vm._v("Spam")
                  ])
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-fce2eefa", module.exports)
  }
}

/***/ })

});