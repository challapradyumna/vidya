webpackJsonp([15],{

/***/ 138:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__model__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_PartyType_js__ = __webpack_require__(138);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }




var PartyType = function (_Model) {
    _inherits(PartyType, _Model);

    function PartyType() {
        _classCallCheck(this, PartyType);

        var _this = _possibleConstructorReturn(this, (PartyType.__proto__ || Object.getPrototypeOf(PartyType)).call(this));

        _this.name = null;
        _this.id = null;
        _this.parent_id = _this;
        _this.url = '/api/party_types';
        _this.searchResultTerm = ['name'];
        _this.tableWith = [];
        _this.columns = [{
            name: 'name',
            type: 'text',
            label: 'Name',
            readOnly: false
        }, {
            name: 'id',
            type: 'number',
            label: 'Id',
            readOnly: true
        }, {
            name: 'parent_id',
            type: 'number',
            label: 'Parent',
            relation: _this
        }];

        _this.fillTableWith();
        return _this;
    }

    _createClass(PartyType, [{
        key: 'selfReference',
        value: function selfReference() {
            var _this2 = this;

            var col = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

            if (col == null) {
                this.columns.forEach(function (col) {
                    if (col.hasOwnProperty('relation')) {
                        if (col.relation === _this2) {
                            _this2[col.name] = new PartyType();
                        }
                    }
                });
            } else {
                this[col.name] = new PartyType();
            }
        }
    }]);

    return PartyType;
}(__WEBPACK_IMPORTED_MODULE_0__model__["a" /* default */]);

/* harmony default export */ __webpack_exports__["a"] = (PartyType);

/***/ }),

/***/ 139:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Model = function () {
    function Model() {
        _classCallCheck(this, Model);

        this.created_at = null;
        this.updated_at = null;
        this.deleted_at = null;
    }

    _createClass(Model, [{
        key: 'fillTableWith',
        value: function fillTableWith() {
            var _this = this;

            this.columns.forEach(function (col) {
                if (col.hasOwnProperty('relation')) {
                    _this.tableWith.push(col.relationWith);
                }
            });
        }
    }, {
        key: 'getPage',
        value: function getPage() {
            var page = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;
            var tableWith = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];

            return axios.get(this.url, {
                params: {
                    page: page,
                    with: this.tableWith.join(',')
                }
            });
        }
    }, {
        key: 'get',
        value: function get(id) {
            var _this2 = this;

            if (id) {
                return axios.get(this.url + "/" + id).then(function (response) {
                    _this2.columns.forEach(function (col) {
                        if (col.hasOwnProperty('relation')) {
                            if (col.relation === _this2) {
                                _this2.selfReference(col);
                            }
                            _this2[col.name].id = response.data[col.name];
                            _this2[col.name].get(response.data[col.name]);
                        } else {
                            _this2[col.name] = response.data[col.name];
                        }
                    });
                    return response;
                });
            }
        }
    }, {
        key: 'datatableSearch',
        value: function datatableSearch(term) {
            return axios.get(this.url, {
                params: {
                    search: term,
                    with: this.tableWith.join(',')
                }
            });
        }
    }, {
        key: 'search',
        value: function search(term) {
            var _this3 = this;

            return axios.get(this.url, {
                params: {
                    search: term
                }
            }).then(function (response) {
                return _this3.getSearchTerms(response.data.data);
            });
        }
    }, {
        key: 'getSearchTerms',
        value: function getSearchTerms(object) {
            var _this4 = this;

            var searchTerms = [];
            if (object == null) {
                return searchTerms;
            }
            object.forEach(function (row) {
                var Term = {};
                Term.name = '';
                Term.id = row.id;
                _this4.searchResultTerm.forEach(function (searchTerm) {
                    Term.name = Term.name + row[searchTerm] + " ";
                });
                searchTerms.push(Term);
            });
            return searchTerms;
        }
    }, {
        key: 'save',
        value: function save() {
            var _this5 = this;

            var postData = {};
            this.columns.forEach(function (col) {
                if (col.hasOwnProperty('relation')) {
                    postData[col.name] = _this5[col.name].id;
                } else {

                    postData[col.name] = _this5[col.name];
                }
            });
            var sendUrl = '';
            if (this.id != null) {
                sendUrl = this.url + '/' + this.id;
                return axios.put(sendUrl, postData).then(function (response) {
                    console.log(response);
                }).catch(function (error) {
                    console.log(error);
                });
            } else {
                sendUrl = this.url;
                return axios.post(sendUrl, postData).then(function (response) {
                    console.log(response);
                }).catch(function (error) {
                    console.log(error);
                });
            }
        }
    }]);

    return Model;
}();

/* harmony default export */ __webpack_exports__["a"] = (Model);

/***/ }),

/***/ 191:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__PartyType__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__model__ = __webpack_require__(139);
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }




var Party = function (_Model) {
    _inherits(Party, _Model);

    function Party() {
        _classCallCheck(this, Party);

        var _this = _possibleConstructorReturn(this, (Party.__proto__ || Object.getPrototypeOf(Party)).call(this));

        _this.id = null;
        _this.name = null;
        _this.party_type_id = new __WEBPACK_IMPORTED_MODULE_0__PartyType__["a" /* default */]();
        _this.searchResultTerm = ['name'];
        _this.url = '/api/parties';
        _this.tableWith = [];
        _this.columns = [{
            name: 'id',
            type: 'number',
            readOnly: true,
            label: 'ID'
        }, {
            name: 'name',
            type: 'text',
            label: 'Name'
        }, {
            name: 'created_at',
            type: 'datetime-local',
            label: 'Created At',
            readOnly: true
        }, {
            name: 'party_type_id',
            type: 'number',
            label: 'Party Type',
            relationWith: 'partyType',
            relation: new __WEBPACK_IMPORTED_MODULE_0__PartyType__["a" /* default */]()
        }];

        _this.fillTableWith();
        return _this;
    }

    return Party;
}(__WEBPACK_IMPORTED_MODULE_1__model__["a" /* default */]);

/* harmony default export */ __webpack_exports__["a"] = (Party);

/***/ }),

/***/ 207:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__model__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Party__ = __webpack_require__(191);
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }




var PartyIdentifier = function (_Model) {
    _inherits(PartyIdentifier, _Model);

    function PartyIdentifier() {
        _classCallCheck(this, PartyIdentifier);

        var _this = _possibleConstructorReturn(this, (PartyIdentifier.__proto__ || Object.getPrototypeOf(PartyIdentifier)).call(this));

        _this.id = null;
        _this.name = null;
        _this.description = null;
        _this.issued_by = new __WEBPACK_IMPORTED_MODULE_1__Party__["a" /* default */]();
        _this.url = '/api/party_identifiers';
        _this.searchResultTerm = ['name'];
        _this.tableWith = [];
        _this.columns = [{
            name: "id",
            type: "number",
            label: "ID",
            readOnly: true
        }, {
            name: "name",
            type: "text",
            label: "Name",
            readOnly: false
        }, {
            name: "description",
            type: "text",
            label: "Description",
            readOnly: false
        }, {
            name: "issued_by",
            type: "number",
            label: "Identifier Issued by",
            readOnly: false,
            relationWith: 'issuedBy',
            relation: new __WEBPACK_IMPORTED_MODULE_1__Party__["a" /* default */]()
        }];

        _this.fillTableWith();
        return _this;
    }

    return PartyIdentifier;
}(__WEBPACK_IMPORTED_MODULE_0__model__["a" /* default */]);

/* harmony default export */ __webpack_exports__["a"] = (PartyIdentifier);

/***/ }),

/***/ 228:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(229)
}
var normalizeComponent = __webpack_require__(3)
/* script */
var __vue_script__ = __webpack_require__(231)
/* template */
var __vue_template__ = __webpack_require__(232)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\js\\components\\party_identifiers\\party_identifiers.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-166a9902", Component.options)
  } else {
    hotAPI.reload("data-v-166a9902", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 229:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(230);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(2)("3bb64973", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-166a9902\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./party_identifiers.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-166a9902\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./party_identifiers.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 230:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(1)(false);
// imports


// module
exports.push([module.i, "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", ""]);

// exports


/***/ }),

/***/ 231:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_datatable_datatable_vue__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_datatable_datatable_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__components_datatable_datatable_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_PartyIdentifier_js__ = __webpack_require__(207);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      object: new __WEBPACK_IMPORTED_MODULE_1__models_PartyIdentifier_js__["a" /* default */]()
    };
  },
  components: {
    'datatable': __WEBPACK_IMPORTED_MODULE_0__components_datatable_datatable_vue___default.a
  },
  methods: {
    viewItem: function viewItem(id) {
      this.$router.push("/party_identifiers/" + id);
    }
  },
  created: function created() {}
});

/***/ }),

/***/ 232:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("datatable", {
        attrs: { object: this.object },
        scopedSlots: _vm._u([
          {
            key: "row",
            fn: function(item) {
              return [
                _c(
                  "md-table-cell",
                  {
                    attrs: {
                      "md-label": "ID",
                      "md-sort-by": "id",
                      "md-numeric": ""
                    }
                  },
                  [_vm._v(_vm._s(item.itemRow.id))]
                ),
                _vm._v(" "),
                _c(
                  "md-table-cell",
                  { attrs: { "md-label": "Name", "md-sort-by": "name" } },
                  [_vm._v(_vm._s(item.itemRow.name))]
                ),
                _vm._v(" "),
                _c(
                  "md-table-cell",
                  {
                    attrs: {
                      "md-label": "Issued By",
                      "md-sort-by": "issued_by"
                    }
                  },
                  [_vm._v(_vm._s(item.itemRow.issued_by.name))]
                ),
                _vm._v(" "),
                _c(
                  "md-table-cell",
                  { attrs: { "md-label": "Actions" } },
                  [
                    _c(
                      "md-button",
                      {
                        on: {
                          click: function($event) {
                            _vm.viewItem(item.itemRow.id)
                          }
                        }
                      },
                      [_c("md-icon", [_vm._v("pageview")])],
                      1
                    )
                  ],
                  1
                )
              ]
            }
          }
        ])
      })
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-166a9902", module.exports)
  }
}

/***/ })

});