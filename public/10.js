webpackJsonp([10],{

/***/ 262:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(270)
}
var normalizeComponent = __webpack_require__(4)
/* script */
var __vue_script__ = __webpack_require__(272)
/* template */
var __vue_template__ = __webpack_require__(273)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\js\\components\\form\\form.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-f21eecd6", Component.options)
  } else {
    hotAPI.reload("data-v-f21eecd6", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 270:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(271);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(3)("c9bcd2dc", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-f21eecd6\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./form.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-f21eecd6\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./form.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 271:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(2)(false);
// imports


// module
exports.push([module.i, "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", ""]);

// exports


/***/ }),

/***/ 272:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

var Form = function Form() {
    _classCallCheck(this, Form);
};

/* harmony default export */ __webpack_exports__["default"] = ({
    props: ['object'],
    data: function data() {
        return {
            sending: false,
            form: null,
            searchData: [],
            currentFormItem: null
        };
    },
    methods: {
        ifError: function ifError(column) {
            var colName = column.name;
            console.log(this.object.colName == null);
            return this.object.colName == null;
        },
        searchColumn: function searchColumn(column) {
            var _this = this;

            this.currentFormItem = column.name;
            this.searchData = [];
            column.relation.search(this.object[column.name].name).then(function (response) {
                _this.searchData = response;
            });
        },
        selectedRow: function selectedRow(item) {
            this.object[this.currentFormItem].name = item.name;
            this.object[this.currentFormItem].id = item.id;
        },
        validateObject: function validateObject() {
            this.object.save();
        }
    },
    created: function created() {
        if (this.$route.params.hasOwnProperty('id')) {
            this.object.get(this.$route.params.id);
        }
    }
});

/***/ }),

/***/ 273:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c(
      "form",
      {
        staticClass: "md-layout",
        attrs: { novalidate: "" },
        on: {
          submit: function($event) {
            $event.preventDefault()
            return _vm.validateObject($event)
          }
        }
      },
      [
        _c("div", { staticClass: "md-layout md-gutter" }, [
          _c(
            "div",
            { staticClass: "md-layout-item" },
            [
              _vm._l(_vm.object.columns, function(column) {
                return !column.readOnly
                  ? _c(
                      "div",
                      { key: column.name },
                      [
                        column.type == "text"
                          ? _c(
                              "md-field",
                              [
                                _c("label", { attrs: { for: "column.name" } }, [
                                  _vm._v(_vm._s(column.label))
                                ]),
                                _vm._v(" "),
                                _c("md-input", {
                                  attrs: {
                                    name: "column.name",
                                    id: "column.name",
                                    disabled: _vm.sending
                                  },
                                  model: {
                                    value: _vm.object[column.name],
                                    callback: function($$v) {
                                      _vm.$set(_vm.object, column.name, $$v)
                                    },
                                    expression: "object[column.name]"
                                  }
                                })
                              ],
                              1
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        column.hasOwnProperty("relation")
                          ? _c(
                              "md-autocomplete",
                              {
                                attrs: { "md-options": _vm.searchData },
                                on: {
                                  "md-changed": function($event) {
                                    _vm.searchColumn(column)
                                  },
                                  "md-opened": function($event) {
                                    _vm.searchColumn(column)
                                  },
                                  "md-selected": _vm.selectedRow
                                },
                                scopedSlots: _vm._u([
                                  {
                                    key: "md-autocomplete-item",
                                    fn: function(ref) {
                                      var item = ref.item
                                      return [_vm._v(_vm._s(item.name))]
                                    }
                                  }
                                ]),
                                model: {
                                  value: _vm.object[column.name].name,
                                  callback: function($$v) {
                                    _vm.$set(
                                      _vm.object[column.name],
                                      "name",
                                      $$v
                                    )
                                  },
                                  expression: "object[column.name].name"
                                }
                              },
                              [_c("label", [_vm._v(_vm._s(column.label))])]
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        column.type == "datetime-local"
                          ? _c(
                              "md-datepicker",
                              {
                                model: {
                                  value: _vm.object[column.name],
                                  callback: function($$v) {
                                    _vm.$set(_vm.object, column.name, $$v)
                                  },
                                  expression: "object[column.name]"
                                }
                              },
                              [_c("label", [_vm._v(_vm._s(column.label))])]
                            )
                          : _vm._e()
                      ],
                      1
                    )
                  : _vm._e()
              }),
              _vm._v(" "),
              _c(
                "md-button",
                {
                  staticClass: "md-raised md-primary",
                  on: {
                    click: function($event) {
                      _vm.object.save()
                    }
                  }
                },
                [_vm._v("Save")]
              ),
              _vm._v(" "),
              _c(
                "md-button",
                {
                  staticClass: "md-raised md-accent",
                  on: {
                    click: function($event) {
                      _vm.$router.go(-1)
                    }
                  }
                },
                [_vm._v("Cancel")]
              )
            ],
            2
          )
        ])
      ]
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-f21eecd6", module.exports)
  }
}

/***/ })

});