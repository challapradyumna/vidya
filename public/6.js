webpackJsonp([6],{

/***/ 264:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(292)
}
var normalizeComponent = __webpack_require__(4)
/* script */
var __vue_script__ = __webpack_require__(294)
/* template */
var __vue_template__ = __webpack_require__(295)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources\\assets\\js\\components\\party_identifier_values\\party_identifier_values.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-42865cdb", Component.options)
  } else {
    hotAPI.reload("data-v-42865cdb", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 267:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Model = function () {
    function Model() {
        _classCallCheck(this, Model);

        this.created_at = null;
        this.updated_at = null;
        this.deleted_at = null;
    }

    _createClass(Model, [{
        key: 'fillTableWith',
        value: function fillTableWith() {
            var _this = this;

            this.columns.forEach(function (col) {
                if (col.hasOwnProperty('relation')) {
                    _this.tableWith.push(col.relationWith);
                }
            });
        }
    }, {
        key: 'getPage',
        value: function getPage() {
            var page = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;
            var tableWith = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];

            return axios.get(this.url, {
                params: {
                    page: page,
                    with: this.tableWith.join(';')
                }
            });
        }
    }, {
        key: 'get',
        value: function get(id) {
            var _this2 = this;

            if (id) {
                return axios.get(this.url + "/" + id).then(function (response) {
                    _this2.columns.forEach(function (col) {
                        if (col.hasOwnProperty('relation')) {
                            if (col.relation === _this2) {
                                _this2.selfReference(col);
                            }
                            _this2[col.name].id = response.data[col.name];
                            _this2[col.name].get(response.data[col.name]);
                        } else {
                            _this2[col.name] = response.data[col.name];
                        }
                    });
                    return response;
                });
            }
        }
    }, {
        key: 'datatableSearch',
        value: function datatableSearch(term) {
            return axios.get(this.url, {
                params: {
                    search: term,
                    with: this.tableWith.join(',')
                }
            });
        }
    }, {
        key: 'search',
        value: function search(term) {
            var _this3 = this;

            return axios.get(this.url, {
                params: {
                    search: term
                }
            }).then(function (response) {
                return _this3.getSearchTerms(response.data.data);
            });
        }
    }, {
        key: 'getSearchTerms',
        value: function getSearchTerms(object) {
            var _this4 = this;

            var searchTerms = [];
            if (object == null) {
                return searchTerms;
            }
            object.forEach(function (row) {
                var Term = {};
                Term.name = '';
                Term.id = row.id;
                _this4.searchResultTerm.forEach(function (searchTerm) {
                    Term.name = Term.name + row[searchTerm] + " ";
                });
                searchTerms.push(Term);
            });
            return searchTerms;
        }
    }, {
        key: 'save',
        value: function save() {
            var _this5 = this;

            var postData = {};
            this.columns.forEach(function (col) {
                if (col.hasOwnProperty('relation')) {
                    postData[col.name] = _this5[col.name].id;
                } else {
                    if (col.type == 'datetime-local') {
                        postData[col.name] = moment(_this5[col.name]).format('YYYY-MM-DD HH:mm:ss');
                    } else {
                        postData[col.name] = _this5[col.name];
                    }
                }
            });
            var sendUrl = '';
            if (this.id != null) {
                sendUrl = this.url + '/' + this.id;
                return axios.put(sendUrl, postData).then(function (response) {
                    console.log(response);
                }).catch(function (error) {
                    console.log(error);
                });
            } else {
                sendUrl = this.url;
                return axios.post(sendUrl, postData).then(function (response) {
                    console.log(response);
                }).catch(function (error) {
                    console.log(error);
                });
            }
        }
    }]);

    return Model;
}();

/* harmony default export */ __webpack_exports__["a"] = (Model);

/***/ }),

/***/ 268:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__model__ = __webpack_require__(267);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_PartyType_js__ = __webpack_require__(268);
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }




var PartyType = function (_Model) {
    _inherits(PartyType, _Model);

    function PartyType() {
        _classCallCheck(this, PartyType);

        var _this = _possibleConstructorReturn(this, (PartyType.__proto__ || Object.getPrototypeOf(PartyType)).call(this));

        _this.name = null;
        _this.id = null;
        _this.parent_id = _this;
        _this.url = '/api/party_types';
        _this.searchResultTerm = ['name'];
        _this.tableWith = [];
        _this.columns = [{
            name: 'name',
            type: 'text',
            label: 'Name',
            readOnly: false
        }, {
            name: 'id',
            type: 'number',
            label: 'Id',
            readOnly: true
        }, {
            name: 'parent_id',
            type: 'number',
            label: 'Parent',
            relationWith: 'partyType',
            relation: _this
        }];

        _this.fillTableWith();
        return _this;
    }

    _createClass(PartyType, [{
        key: 'selfReference',
        value: function selfReference() {
            var _this2 = this;

            var col = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

            if (col == null) {
                this.columns.forEach(function (col) {
                    if (col.hasOwnProperty('relation')) {
                        if (col.relation === _this2) {
                            _this2[col.name] = new PartyType();
                        }
                    }
                });
            } else {
                this[col.name] = new PartyType();
            }
        }
    }]);

    return PartyType;
}(__WEBPACK_IMPORTED_MODULE_0__model__["a" /* default */]);

/* harmony default export */ __webpack_exports__["a"] = (PartyType);

/***/ }),

/***/ 269:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__PartyType__ = __webpack_require__(268);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__model__ = __webpack_require__(267);
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }




var Party = function (_Model) {
    _inherits(Party, _Model);

    function Party() {
        _classCallCheck(this, Party);

        var _this = _possibleConstructorReturn(this, (Party.__proto__ || Object.getPrototypeOf(Party)).call(this));

        _this.id = null;
        _this.name = null;
        _this.party_type_id = new __WEBPACK_IMPORTED_MODULE_0__PartyType__["a" /* default */]();
        _this.searchResultTerm = ['name'];
        _this.url = '/api/parties';
        _this.tableWith = [];
        _this.columns = [{
            name: 'id',
            type: 'number',
            readOnly: true,
            label: 'ID'
        }, {
            name: 'name',
            type: 'text',
            label: 'Name'
        }, {
            name: 'party_type_id',
            type: 'number',
            label: 'Party Type',
            relationWith: 'partyType',
            relation: new __WEBPACK_IMPORTED_MODULE_0__PartyType__["a" /* default */]()
        }];

        _this.fillTableWith();
        return _this;
    }

    return Party;
}(__WEBPACK_IMPORTED_MODULE_1__model__["a" /* default */]);

/* harmony default export */ __webpack_exports__["a"] = (Party);

/***/ }),

/***/ 274:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__model__ = __webpack_require__(267);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Party__ = __webpack_require__(269);
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }




var PartyIdentifier = function (_Model) {
    _inherits(PartyIdentifier, _Model);

    function PartyIdentifier() {
        _classCallCheck(this, PartyIdentifier);

        var _this = _possibleConstructorReturn(this, (PartyIdentifier.__proto__ || Object.getPrototypeOf(PartyIdentifier)).call(this));

        _this.id = null;
        _this.name = null;
        _this.description = null;
        _this.issued_by = new __WEBPACK_IMPORTED_MODULE_1__Party__["a" /* default */]();
        _this.url = '/api/party_identifiers';
        _this.searchResultTerm = ['name'];
        _this.tableWith = [];
        _this.columns = [{
            name: "id",
            type: "number",
            label: "ID",
            readOnly: true
        }, {
            name: "name",
            type: "text",
            label: "Name",
            readOnly: false
        }, {
            name: "description",
            type: "text",
            label: "Description",
            readOnly: false
        }, {
            name: "issued_by",
            type: "number",
            label: "Identifier Issued by",
            readOnly: false,
            relationWith: 'issuedBy',
            relation: new __WEBPACK_IMPORTED_MODULE_1__Party__["a" /* default */]()
        }];

        _this.fillTableWith();
        return _this;
    }

    return PartyIdentifier;
}(__WEBPACK_IMPORTED_MODULE_0__model__["a" /* default */]);

/* harmony default export */ __webpack_exports__["a"] = (PartyIdentifier);

/***/ }),

/***/ 275:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__model__ = __webpack_require__(267);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Party_js__ = __webpack_require__(269);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__PartyIdentifier_js__ = __webpack_require__(274);
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }





var PartyIdentifierValue = function (_Model) {
    _inherits(PartyIdentifierValue, _Model);

    function PartyIdentifierValue() {
        _classCallCheck(this, PartyIdentifierValue);

        var _this = _possibleConstructorReturn(this, (PartyIdentifierValue.__proto__ || Object.getPrototypeOf(PartyIdentifierValue)).call(this));

        _this.id = null;
        _this.value = null;
        _this.begin = null;
        _this.end = null;
        _this.comment = null;
        _this.from = new __WEBPACK_IMPORTED_MODULE_1__Party_js__["a" /* default */]();
        _this.to = new __WEBPACK_IMPORTED_MODULE_1__Party_js__["a" /* default */]();
        _this.party_identifier_id = new __WEBPACK_IMPORTED_MODULE_2__PartyIdentifier_js__["a" /* default */]();
        _this.url = '/api/party_identifier_values';
        _this.searchResultTerm = ['value'];
        _this.relationWith = 'partyIdentifierValue';
        _this.tableWith = [];
        _this.columns = [{
            name: "id",
            type: "number",
            label: "ID",
            readOnly: true
        }, {
            name: "value",
            type: "text",
            label: "Value",
            readOnly: false
        }, {
            name: "begin",
            type: "datetime-local",
            label: "Valid from",
            readOnly: false
        }, {
            name: "end",
            type: "datetime-local",
            label: "Valid to",
            readOnly: false
        }, {
            name: "from",
            type: "number",
            label: "Identifier Issued From",
            readOnly: false,
            relationWith: 'from',
            relation: new __WEBPACK_IMPORTED_MODULE_1__Party_js__["a" /* default */]()
        }, {
            name: "to",
            type: "number",
            label: "Identifier Issued to",
            readOnly: false,
            relationWith: 'to',
            relation: new __WEBPACK_IMPORTED_MODULE_1__Party_js__["a" /* default */]()
        }, {
            name: "party_identifier_id",
            type: "number",
            label: "Party Identifier Details",
            relationWith: 'partyIdentifier',
            readOnly: false,
            relation: new __WEBPACK_IMPORTED_MODULE_2__PartyIdentifier_js__["a" /* default */]()
        }];

        _this.fillTableWith();
        return _this;
    }

    return PartyIdentifierValue;
}(__WEBPACK_IMPORTED_MODULE_0__model__["a" /* default */]);

/* harmony default export */ __webpack_exports__["a"] = (PartyIdentifierValue);

/***/ }),

/***/ 292:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(293);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(3)("33932c2a", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-42865cdb\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./party_identifier_values.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-42865cdb\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./party_identifier_values.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 293:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(2)(false);
// imports


// module
exports.push([module.i, "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", ""]);

// exports


/***/ }),

/***/ 294:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_datatable_datatable_vue__ = __webpack_require__(134);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_datatable_datatable_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__components_datatable_datatable_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_PartyIdentifierValue_js__ = __webpack_require__(275);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      object: new __WEBPACK_IMPORTED_MODULE_1__models_PartyIdentifierValue_js__["a" /* default */]()
    };
  },
  components: {
    'datatable': __WEBPACK_IMPORTED_MODULE_0__components_datatable_datatable_vue___default.a
  },
  methods: {
    viewItem: function viewItem(id) {
      this.$router.push("/party_identifier_values/" + id);
    }
  },
  created: function created() {}
});

/***/ }),

/***/ 295:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("datatable", {
        attrs: { object: this.object },
        scopedSlots: _vm._u([
          {
            key: "row",
            fn: function(item) {
              return [
                _c(
                  "md-table-cell",
                  {
                    attrs: {
                      "md-label": "ID",
                      "md-sort-by": "id",
                      "md-numeric": ""
                    }
                  },
                  [_vm._v(_vm._s(item.itemRow.id))]
                ),
                _vm._v(" "),
                _c(
                  "md-table-cell",
                  { attrs: { "md-label": "Value", "md-sort-by": "name" } },
                  [_vm._v(_vm._s(item.itemRow.value))]
                ),
                _vm._v(" "),
                _c(
                  "md-table-cell",
                  {
                    attrs: { "md-label": "Issued From", "md-sort-by": "from" }
                  },
                  [_vm._v(_vm._s(item.itemRow.from.name))]
                ),
                _vm._v(" "),
                _c(
                  "md-table-cell",
                  { attrs: { "md-label": "Issued To", "md-sort-by": "to" } },
                  [_vm._v(_vm._s(item.itemRow.to.name))]
                ),
                _vm._v(" "),
                _c(
                  "md-table-cell",
                  {
                    attrs: { "md-label": "Valid Begin", "md-sort-by": "begin" }
                  },
                  [_vm._v(_vm._s(item.itemRow.begin))]
                ),
                _vm._v(" "),
                _c(
                  "md-table-cell",
                  { attrs: { "md-label": "Valid End", "md-sort-by": "end" } },
                  [_vm._v(_vm._s(item.itemRow.end))]
                ),
                _vm._v(" "),
                _c(
                  "md-table-cell",
                  {
                    attrs: {
                      "md-label": "Identifier",
                      "md-sort-by": "party_identifier_id"
                    }
                  },
                  [_vm._v(_vm._s(item.itemRow.party_identifier.name))]
                ),
                _vm._v(" "),
                _c(
                  "md-table-cell",
                  { attrs: { "md-label": "Actions" } },
                  [
                    _c(
                      "md-button",
                      {
                        on: {
                          click: function($event) {
                            _vm.viewItem(item.itemRow.id)
                          }
                        }
                      },
                      [_c("md-icon", [_vm._v("pageview")])],
                      1
                    )
                  ],
                  1
                )
              ]
            }
          }
        ])
      })
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-42865cdb", module.exports)
  }
}

/***/ })

});