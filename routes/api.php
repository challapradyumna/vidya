<?php

use Illuminate\Http\Request;
use App\Http\Controllers\API\PartyAPIController;
use App\Http\Controllers\API\PartyTypeAPIController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/', function () {
    return "Welcome to API";
});

Route::resource('parties', 'API\PartyAPIController');

Route::resource('party_types', 'API\PartyTypeAPIController');

Route::resource('party_identifier_values', 'API\PartyIdentifierValueAPIController');

Route::resource('party_identifiers', 'API\PartyIdentifierAPIController');

Route::resource('party_identifier_assignments', 'API\PartyIdentifierAssignmentAPIController');

Route::resource('party_characteristic_values', 'API\PartyCharacteristicValueAPIController');

Route::resource('party_characteristics', 'API\PartyCharacteristicAPIController');

Route::resource('party_characteristic_assignments', 'API\PartyCharacteristicAssignmentAPIController');

Route::resource('party_relationships', 'API\PartyRelationshipAPIController');

Route::resource('party_relationship_types', 'API\PartyRelationshipTypeAPIController');

Route::resource('courses', 'API\CourseAPIController');

Route::resource('subjects', 'API\SubjectAPIController');

Route::resource('course_students', 'API\CourseStudentAPIController');

Route::resource('student_subjects', 'API\StudentSubjectAPIController');