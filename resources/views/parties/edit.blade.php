@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Party
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($party, ['route' => ['parties.update', $party->id], 'method' => 'patch']) !!}

                        @include('parties.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection