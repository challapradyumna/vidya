import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from "@/components/login.vue";
import Logout from "@/components/logout.vue";
import App from "@/components/layouts/app.vue";
import Home from "@/components/home.vue";
// import Party from "@/components/party.vue";
const AppHeader = () => import('@/components/layouts/app-header.vue')
const AppFooter = () => import('@/components/layouts/app-footer.vue')
import store from '@/state/store'


Vue.component('app',App);
Vue.component('home',Home);
Vue.component('login',Login);
Vue.component('logout',Logout);
Vue.component('AppHeader',AppHeader);
Vue.component('AppFooter',AppFooter);
Vue.component('passport-clients',require('@/components/passport/Clients.vue'));
Vue.component('passport-authorized-clients',require('@/components/passport/AuthorizedClients.vue'));
Vue.component('passport-personal-access-tokens',require('@/components/passport/PersonalAccessTokens.vue'));

Vue.use(VueRouter)
const routes = [
  {path:'/',component:Login},
  {path:'/home',component:Home,name:'home'},
  {path:'/logout',component:Logout},
  // {path:'/party/:id',component:Party}
]

function crudRoutes(routes)
{
  let models = ['party','party_types','party_identifier_values','party_identifiers'];
  let crudRoutes = ['','/create','/:id','/:id/edit'];
  models.forEach((model)=>{
    const Model = () => import('@/components/'+model+'/'+model+'.vue')
    const ModelForm = () => import('@/components/'+model+'/'+model+'_form.vue')
    crudRoutes.forEach((crud)=>{
      let r = {};
      r.path = '/'+model+crud;
      if(crud==''){
        r.component = Model;
      }else{
        r.component = ModelForm;
      }
      routes.push(r);
    })
  })
}
crudRoutes(routes);

  const router = new VueRouter({
    routes // short for `routes: routes`
  })

  router.beforeEach((to, from, next) => {
    if(to.path == '/'&&store.getters["auth/loggedIn"]){ 
      next({ path:'/home'})
    }
    else if(to.path !== '/'&&!store.getters["auth/loggedIn"]){
      next({ path:'/'})
    }
    else if(store.getters["auth/loggedIn"]){
      next()
    }
    else{
      next()
    }
    
  })
  export default router