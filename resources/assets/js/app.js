import VueMaterial from 'vue-material'
import store from './state/store'
import router from './routes'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'

require('./bootstrap');

window.Vue = require('vue');
Vue.use(VueMaterial) 

const app = new Vue({
    el: '#app',
    data: {
    },
    store,
    router
});
