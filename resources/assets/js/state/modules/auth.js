import axios from 'axios'

export const state = {
  currentUser: getSavedState('auth.currentUser'),
  tokens:getSavedState('auth.tokens'),
}

export const mutations = {
  SET_CURRENT_USER(state, newValue) {
    state.currentUser = newValue
    saveState('auth.currentUser', newValue)
  },
  SET_TOKENS(state,newValue){
    state.tokens = newValue
    saveState('auth.tokens',newValue)
    setDefaultAuthHeaders(state)
  }
}

export const getters = {
  // Whether the user is currently logged in.
  loggedIn(state) {
    return !!state.currentUser
  },
}

export const actions = {
  // This is automatically run in `src/state/store.js` when the app
  // starts, along with any other actions named `init` in other modules.
  init({ state, dispatch }) {
    setDefaultAuthHeaders(state)
    dispatch('validate')
  },

  // Logs in the current user.
  logIn({ commit, dispatch, getters }, { username, password } = {}) {
    if (getters.loggedIn) return dispatch('validate')

    return axios.post('/oauth/token', {
                grant_type: 'password',
                client_id: '2',
                client_secret:'ZHlRZZajjED4jxvtOYcdEA2QsbrJHEPQWTwlJ4mZ',
                username,
                password
    }).then(response => {
      const user = response.data
      commit('SET_TOKENS', user)
      dispatch('validate')
      return user
    })
  },

  // Logs out the current user.
  logOut({ commit }) {
    commit('SET_CURRENT_USER', null)
    commit('SET_TOKENS', null)
  },

  // Validates the current user's token and refreshes it
  // with new data from the API.
  validate({ commit, state }) {
    if (!state.tokens) return Promise.resolve(null)

    return axios
      .get('/api/user')
      .then(response => {
        const user = response.data
        commit('SET_CURRENT_USER', user)
        return user
      })
      .catch(error => {
        if (error.response.status === 401) {
          commit('SET_CURRENT_USER', null)
        }
        return null
      })
  },
}

// ===
// Private helpers
// ===

function getSavedState(key) {
  return JSON.parse(window.localStorage.getItem(key))
}

function saveState(key, state) {
  window.localStorage.setItem(key, JSON.stringify(state))
}

function setDefaultAuthHeaders(state) {
  axios.defaults.headers.common.Authorization = state.tokens
    ? "Bearer "+state.tokens.access_token
    : ''
}