import Model from "./model";
import Party from "./Party.js";
import PartyIdentifier from "./PartyIdentifier.js"

export default class PartyIdentifierValue extends Model{

    id=null;
    value=null;
    begin=null;
    end=null;
    comment=null;
    from=new Party();
    to=new Party();
    party_identifier_id=new PartyIdentifier();

    url='/api/party_identifier_values'
    searchResultTerm = ['value'];
    relationWith='partyIdentifierValue';
    tableWith=[];
    columns=[
        {
            name:"id",
            type:"number",
            label:"ID",
            readOnly:true,
        },
        {
            name:"value",
            type:"text",
            label:"Value",
            readOnly:false,
        },
        {
            name:"begin",
            type:"datetime-local",
            label:"Valid from",
            readOnly:false,
        },
        {
            name:"end",
            type:"datetime-local",
            label:"Valid to",
            readOnly:false,
        },
        {
            name:"from",
            type:"number",
            label:"Identifier Issued From",
            readOnly:false,
            relationWith:'from',
            relation:new Party()
        },
        {
            name:"to",
            type:"number",
            label:"Identifier Issued to",
            readOnly:false,
            relationWith:'to',
            relation:new Party()
        },
        {
            name:"party_identifier_id",
            type:"number",
            label:"Party Identifier Details",
            relationWith:'partyIdentifier',
            readOnly:false,
            relation:new PartyIdentifier()
        },
    ]

    constructor(){
        super()
        this.fillTableWith();
    }
}
