import Model from "./model";
import Party from "./Party";

export default class PartyIdentifier extends Model{

    id=null;
    name=null;
    description=null;
    issued_by=new Party();
    url='/api/party_identifiers'
    searchResultTerm = ['name'];
    tableWith=[];
    columns=[
        {
            name:"id",
            type:"number",
            label:"ID",
            readOnly:true,
        },
        {
            name:"name",
            type:"text",
            label:"Name",
            readOnly:false,
        },
        {
            name:"description",
            type:"text",
            label:"Description",
            readOnly:false,
        },
        {
            name:"issued_by",
            type:"number",
            label:"Identifier Issued by",
            readOnly:false,
            relationWith:'issuedBy',
            relation:new Party()
        },
    ]

    constructor(){
        super()
        this.fillTableWith();
    }
}