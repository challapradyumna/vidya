export default class Model{
    created_at=null;
    updated_at=null;
    deleted_at=null;
    constructor(){
        
    }

    fillTableWith()
    {
        this.columns.forEach((col)=>{
            if(col.hasOwnProperty('relation')){
                this.tableWith.push(col.relationWith);
            }
        });
    }

    getPage(page=1,tableWith=[]){
        return axios.get(this.url,{
            params:{
                page:page,
                with:this.tableWith.join(';')
            }
        })
    }

    get(id){
        if(id){
        return axios.get(this.url+"/"+id).then((response)=>{
            this.columns.forEach((col)=>{
                if(col.hasOwnProperty('relation')){
                    if(col.relation === this){
                        this.selfReference(col);
                    }
                    this[col.name].id = response.data[col.name];
                    this[col.name].get(response.data[col.name]);
                }else{
                    this[col.name] = response.data[col.name];
                }
            });
            return response;
        });
        }
    }

    datatableSearch(term){
        return axios.get(this.url,{
            params:{
                search:term,
                with:this.tableWith.join(',')
            }
        })
    }

    search(term){
        return axios.get(this.url,{
            params:{
                search:term,
            }
        }).then((response)=>{
            return this.getSearchTerms(response.data.data);
    });
    }
    getSearchTerms(object){
        var searchTerms = [];
        if(object==null){
            return searchTerms;
        }
        object.forEach((row)=>{
            var Term ={};
            Term.name='';
            Term.id=row.id;
            this.searchResultTerm.forEach((searchTerm)=>{
                Term.name=Term.name+row[searchTerm]+" ";
            })
            searchTerms.push(Term);
        })
        return searchTerms;
    }
    save()
    {
        var postData = {};
        this.columns.forEach((col)=>{
            if(col.hasOwnProperty('relation')){
                postData[col.name] = this[col.name].id;
            }else{
                if(col.type == 'datetime-local'){
                    postData[col.name] = moment(this[col.name]).format('YYYY-MM-DD HH:mm:ss');
                }else{
                    postData[col.name] = this[col.name];
                }
            }
            
        });
        let sendUrl = '';
        if(this.id != null){
            sendUrl = this.url + '/' +this.id;
           return axios.put(sendUrl, postData)
          .then(function (response) {
            console.log(response);
          })
          .catch(function (error) {
            console.log(error);
          });
        }
        else{
            sendUrl = this.url;
           return axios.post(sendUrl, postData)
          .then(function (response) {
            console.log(response);
          })
          .catch(function (error) {
            console.log(error);
          });
        }
    }
}