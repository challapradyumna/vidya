import Model from "./model";
import PType from '@/models/PartyType.js'

export default class PartyType extends Model{
    name=null;
    id=null;
    parent_id=this;
    url='/api/party_types'
    searchResultTerm = ['name'];
    tableWith=[];
    columns=[{
        name:'name',
        type:'text',
        label:'Name',
        readOnly:false
    },{
        name:'id',
        type:'number',
        label:'Id',
        readOnly:true
    },{
        name:'parent_id',
        type:'number',
        label:'Parent',
        relationWith:'partyType',
        relation: this
    }];
    constructor(){
        super()
        this.fillTableWith();
    }

    selfReference(col=null){
        if(col==null){
        this.columns.forEach((col)=>{
            if(col.hasOwnProperty('relation')){
               if(col.relation === this){
                   this[col.name] = new PartyType();
               }
            }
           })
        }
        else{
            this[col.name] = new PartyType();
        }
        
    }
}