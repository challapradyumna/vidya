import PartyType from "./PartyType";
import Model from "./model";

export default class Party extends Model{
   id=null;
   name=null;
   party_type_id=new PartyType();
   searchResultTerm = ['name'];
   url='/api/parties'
   tableWith = [];
   columns = [{
       name:'id',
       type:'number',
       readOnly:true,
       label:'ID'
   },{
    name:'name',
    type:'text',
    label:'Name'
   },{
       name:'party_type_id',
       type:'number',
       label:'Party Type',
       relationWith:'partyType',
       relation:new PartyType(),
   }];

   constructor(){
        super();
        this.fillTableWith();
   }
}